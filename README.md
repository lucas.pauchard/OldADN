OLDADN-I, projet de semestre 6

Ce répertoire Git contient:
- Documentations (CdC, Rapport final, PVs, Directives, Planning)
- Profilage de l'application ATLAS sur différentes tâches
- Sources
    - atlas_seqlib_integration -> ATLAS modifié pour intégrer la librairie SeqLib
    - calcul_losing_precision  -> Programme Java pour calculer la différence de précision
    - samples                  -> Les fichiers BAM et FASTA utilisé pour les tests de l'application
    - test_lib                 -> Tests des librairies SeqLib et Seqan
    - theta_optimisation       -> Optimisations apporté à Theta avec les fichiers modifiés et les tests fast_log