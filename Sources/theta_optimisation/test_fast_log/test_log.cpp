#include <stdio.h>
#include <math.h>
#include <time.h>



static double diff(struct timespec start, struct timespec end);

inline float fast_log2 (float val)
{
   int * const    exp_ptr = reinterpret_cast <int *> (&val);
   int            x = *exp_ptr;
   const int      log_2 = ((x >> 23) & 255) - 128;
   x &= ~(255 << 23);
   x += 127 << 23;
   *exp_ptr = x;

   val = ((-1.0f/3) * val + 2) * val - 2.0f/3;   // (1)

   return (val + log_2);
} 

inline float fast_log (const float &val)
{
   return (fast_log2 (val) * 0.69314718f);
}

int main(int argc, char* argv[]){

 int max = atoi(argv[1]);
 int j;
 double sum=0.0;
 double prod;

 double array[max];
 
 srand((unsigned)time(0)); 
     
 for(int i=0; i<max; i++){ 
     array[i] = ((double) rand() / (RAND_MAX) /2.0);
}

 struct timespec tStart, tEnd;
//-----------------------------------------------------
 sum=0.0;

 
 clock_gettime(CLOCK_REALTIME, &tStart);
 for (j=0; j < max; j++){
  sum+=log(array[j]);
 }
 
 clock_gettime(CLOCK_REALTIME, &tEnd);
 printf ("TEMPS ADD = %f\n", diff(tStart, tEnd));
 printf("Simple ADD: %.15f\n",sum);
//-----------------------------------------------------
 sum=0.0;

 clock_gettime(CLOCK_REALTIME, &tStart);
 for (j=0; j < max; j++){
  sum+=fast_log(array[j]);
 }
 
 clock_gettime(CLOCK_REALTIME, &tEnd);
 printf ("TEMPS ADD = %f\n", diff(tStart, tEnd));
 printf("FAST_LOG ADD: %.15f\n",sum);
//-----------------------------------------------------
 sum=0;
 prod=1.0;

 clock_gettime(CLOCK_REALTIME, &tStart);
 for (j=0; j < max; j++){
   prod*=array[j];
   if(j%20==0){
     sum+=fast_log(prod);
     prod=1.0;
   }
 }
 sum+=fast_log(prod);
 clock_gettime(CLOCK_REALTIME, &tEnd);
 printf ("TEMPS PROD = %f\n", diff(tStart, tEnd));
 printf("FAST_LOG PROD: %.15f\n",sum);
 //----------------------------------------------------
}


double diff(struct timespec start, struct timespec end){
double t1, t2;
 t1 = (double)start.tv_sec;
 t1 = t1 + ((double)start.tv_nsec)/100000.0;
 t2 = (double)end.tv_sec;
 t2 = t2 + ((double)end.tv_nsec)/100000.0;
 return (t2-t1);
}
