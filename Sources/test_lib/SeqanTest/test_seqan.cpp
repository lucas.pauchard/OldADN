#include <iostream>

#include <seqan/basic.h>
#include <seqan/sequence.h>
#include <seqan/stream.h>
#include <seqan/bam_io.h>

using namespace seqan;

int main(int argc, char const ** argv)
{
    CharString bamFileInName = "/root/Desktop/test_tools/sample_sec.bam";

    // Open input BAM file.
    BamFileIn bamFileIn(toCString(bamFileInName));

    return 0;
}
