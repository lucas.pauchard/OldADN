#include "SeqLib/BamReader.h"
#include "SeqLib/BamWriter.h"
#include "SeqLib/BWAWrapper.h"

using namespace SeqLib;

int main(){
    // open the reader BAM/SAM/CRAM
    BamReader bw;
    bw.Open("./sample_sec.bam");

// open a new interface to BWA-MEM
    BWAWrapper bwa;
    bwa.LoadIndex("./sample_sec.fasta");

// open the output BAM
    BamWriter writer; // or writer(SeqLib::SAM) or writer(SeqLib::CRAM)
    writer.SetHeader(bwa.HeaderFromIndex());
    writer.Open("out.bam");
    writer.WriteHeader();

    BamRecord r;
    bool hardclip = false;
    float secondary_cutoff = 0.90; // secondary alignments must have score >= 0.9*top_score
    int secondary_cap = 10; // max number of secondary alignments to return
    while (bw.GetNextRecord(r)) {
        BamRecordVector results; // alignment results (can have multiple alignments)
        bwa.AlignSequence(r.Sequence(), r.Qname(), results, hardclip, secondary_cutoff, secondary_cap);

        for (auto& i : results)
            writer.WriteRecord(i);
    }
}
