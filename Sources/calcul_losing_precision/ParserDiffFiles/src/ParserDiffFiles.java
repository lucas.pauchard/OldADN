
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

public class ParserDiffFiles {

   public static void main(String[] args) {
      //CHECK ARGUMENTS
      if(args.length < 2){
         System.out.println("Invalid arguments length: need two filepaths");
         return;
      }
      
      List<String> file1,file2;
      
      try {
          file1 = Files.readAllLines(Paths.get(args[0]));
      } catch (Exception e) {
         System.out.println("Cannot read file at: "+args[0]);
         return;
      }
      
     try {
         file2 = Files.readAllLines(Paths.get(args[1]));
     } catch (Exception e) {
        System.out.println("Cannot read file at: "+args[1]);
        return;
     }
     
     //CHECK FILES
     System.out.println("Reading files OK");
     System.out.print("Checking file... ");
     
     int nb_field=checkFiles(file1,file2);
     
     if(nb_field<0){
        System.out.println("Check Fail");
        return;
     }
     
     System.out.println("Check OK");
     
     //READ FILE
     System.out.println("Reading files...");
     
     System.out.println(file1.get(0));
     
     NumberFormat formatter = new DecimalFormat("#0.000000000");
     NumberFormat percent = new DecimalFormat("#0.0000000");
     
     double[][] scores = new double[file1.size()][nb_field];
     double[] sum_absolute_error=new double[nb_field];
     double[] sum_relative_error=new double[nb_field];
     double[] biggest=new double[nb_field];
     
     String[] line_file1,line_file2;
     
     int i=1;
     int j;
     
     while(i<file1.size()){
        line_file1=file1.get(i).split("\t");
        line_file2=file2.get(i).split("\t");
        
        for (j = 0; j < line_file2.length; j++) {
           scores[i][j]= Math.abs(Double.parseDouble(line_file1[j]) - Double.parseDouble(line_file2[j]));
           sum_absolute_error[j]+=scores[i][j];
           sum_relative_error[j]+=scores[i][j]/Double.parseDouble(line_file1[j]);
           if(scores[i][j]> biggest[j]){
              biggest[j]=scores[i][j];
           }
           System.out.print(formatter.format(scores[i][j])+"\t");
        }
        System.out.println();
        i++;
     }
     
     //AVERAGE
     System.out.println("Average Absolute Error");
     for (j = 0; j < sum_absolute_error.length; j++) {
        System.out.print(formatter.format(sum_absolute_error[j]/(file1.size()-1))+"\t");
     }
     System.out.println();
     System.out.println("Average Relative Error");
     for (j = 0; j < sum_absolute_error.length; j++) {
        System.out.print(percent.format((sum_relative_error[j]/(file1.size()-1))*100)+"%\t");
     }
     System.out.println();
     //BIGGEST DIFFERENCE
     System.out.println("Biggest Difference");
     for (j = 0; j < biggest.length; j++) {
        System.out.print(formatter.format(biggest[j])+"\t");
     }
     System.out.println();
   }
   
   private static int checkFiles(List<String> file1, List<String> file2){
      if(file1.size()!=file2.size()){
         System.out.println("The two files don't contain the same number of lines.");
         return -1;
      }
      String line1_file1[] = file1.get(0).split("\t");
      String line1_file2[] = file1.get(0).split("\t");
      if(line1_file1.length!=line1_file2.length){
         System.out.println("The two files don't contain the same number of columns. Check the number of tabs.");
         return -1;
      }else{
         return line1_file1.length;
      }
   }
   
   

}
