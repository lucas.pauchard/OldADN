/*
 * TWindowSeqLib.h
 *
 *  Created on: May 7, 2019
 *      Author: pauchardl
 */

#ifndef TWINDOW_H_SEQLIB
#define TWINDOW_H_SEQLIB

#include "TWindow.h"


//---------------------------------------------------------------
//TWindowSeqLib
//---------------------------------------------------------------
class TWindowSeqLib : public TWindow{
private:
	/*TGenotypeMap genoMap;
	bool referenceBaseAdded;

	//alignment stacks
	std::vector<TAlignment*> usedAlignments;
	std::vector<TAlignment*> emptyAlignments;*/

	void fillPGenotype(double* pGenotype);
	void setCoordinates(long Start, long End, int ChrNumber);
	void cleanUpUsedAlignments();
	void clearAllUsedAlignments();
	//std::vector<TAlignment*>::iterator lastAlignmentwithEndInWindow;
	//std::vector<TAlignment*>::iterator firstAlignmentwithPosOutsideWindow;

public:
	/*long start;
	long end; //end NOT included in window!
	long length;
	int chrNumber;
	std::string chrName;
	TSite* sites;
	bool sitesInitialized;
	int numReadsInWindow;
	double depth, fractionSitesNoData, fractionsitesDepthAtLeastTwo;
	double fractionRefIsN;
	long numSitesWithData;
	bool passedFilters;
	TBaseFrequencies baseFreq;*/

	TWindowSeqLib();
	~TWindowSeqLib();

	//getters
	TBaseFrequencies getBaseFreq(){return baseFreq;};

	TAlignment* swapUsedForEmptyAlignment(TAlignment* usedAlignment, const unsigned int & maxReadLength);
	void initSites(long newLength);
	void clear();
	void move(long Start, long End, int chrNumber);
	void jump(long Start, long End, int ChrNumber);
	void review();
	void printStacks();
	void fillSitesSubset(TSiteSubset* subset, const int & readUpToDepth);
	void fillSites(const int & readUpToDepth);
	void addReferenceBaseToSites(BamTools::Fasta & reference);
	void addReferenceBaseToSites(TSiteSubset* subset);
	void applyMask(TBedReader* mask, bool inverseMasking);
	void maskCpG();
	void estimateBaseFrequencies();
	void calculateEmissionProbabilities();
//void callMLEGenotype(TRecalibration* recalObject, TRandomGenerator & randomGenerator, gz::ogzstream & out, std::string & chr, bool printAll, bool printRef, bool isVCF, bool gVCF, bool noAltIfHomoRef);
	void printPileup(TRecalibration* recalObject, gz::ogzstream & out, bool printOnlySitesWithData);
	void printPileupToScreen(TRecalibration* recalObject);
	void calcDepth();
	void calcFracN();
	void calcDepthPerSite(long * siteDepth, size_t maxCov);
	void countDepthPerSite(TDistributionOfCounts & counts);
	void printDepthPerSite(gz::ogzstream & out, std::string & chr);
	void printMateInformationPerSite(TOutputFileZipped & out, std::string & chr);
	void countAlleles(long**** siteImbalance, const unsigned int & maxDepth);
	void applyDepthFilter(const size_t minDepth, const size_t maxDepth);
	void createDepthMask(size_t minDepth, size_t maxDepth, std::ofstream & outputMaskFile, std::string & chr);

	//add sites to data structures
	void addSitesToBQSR(TRecalibrationBQSREstimator & bqsr, TLog* logfile, TQualityMap & qualMap);
	void addSitesToBQSR(TRecalibrationBQSREstimator & bqsr, TSiteSubset* subset, TLog* logfile, TQualityMap & qualMap);
	//void addSitesToQualityTransformTable(TRecalibration* recalObject, std::vector<TQualityTransformTable*> & QTtables, TLog* logfile, TQualityMap & qualMap){};
	//void addSitesToQualityTransformTable(TRecalibration* recalObject, TRecalibration* otherRecalObject, std::vector<TQualityTransformTable*> & QTtables, TLog* logfile, TQualityMap & qualMap){};
	void addSitesToPMDTable(TPMDTables & pmdTables, TLog* logfile);
	void addSitesToThetaEstimator(TThetaEstimatorData* thetaDataContainer);
	void addSitesToThetaEstimator(TThetaEstimatorData* thetaDataContainer, TBedReader & region);
	void addToGLF(TGlfWriter & writer, bool printAll);
	void addToRecalibrationEM(TRecalibrationEMEstimator & recalObject, TQualityMap & qualMap);
	void addToRecalibrationEM(TRecalibrationEMEstimator & recalObject, TSiteSubset* subset, TQualityMap & qualMap);

	//callers
	void call(TCaller & caller, TRecalibration & recalObject, BamTools::Fasta & reference);
	void callKnwonAlleles(TCaller & caller, TRecalibration & recalObject, TSiteSubset & subset);

	//other
	void generatePSMCInput(TThetaEstimator & estimator, int & blockSize, double & confidence, std::ofstream & out, int & nCharOnLine);
	double calcLogLikelihood();
};

#endif /* TWINDOW_H_SEQLIB */
