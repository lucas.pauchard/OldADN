/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   TWindow.h
 * Author: Lucas Pauchard
 *
 * Created on 18. avril 2019, 07:48
 */

#ifndef TWINDOW_H
#define TWINDOW_H

#include "../TLog.h"
#include "../TParameters.h"
#include "../TReadGroups.h"
#include "../Recalibration/TRecalibration.h"
#include "../Recalibration/TRecalibrationBQSR.h"
#include "../Recalibration/TRecalibrationEMEstimator.h"
#include "../TThetaEstimator.h"
#include "../TBedReader.h"
#include "../TSiteSubset.h"
#include "../TPostMortemDamage.h"
#include "../TGLF.h"
#include "../Alignment/TAlignmentBamTools.h"
#include "../Alignment/TAlignmentSeqLib.h"
#include "../TQualityMap.h"
#include "../TCaller.h"    
#include "../TDistributionOfCounts.h"

//-----------------------------------------------------
//TWindow
//-----------------------------------------------------

class TWindow{
protected:
	TGenotypeMap genoMap;
	bool referenceBaseAdded;

	//alignment stacks
	std::vector<TAlignment*> usedAlignments;
	std::vector<TAlignment*> emptyAlignments;

	virtual void fillPGenotype(double* pGenotype)=0;
	virtual void setCoordinates(long Start, long End, int ChrNumber)=0;
	virtual void cleanUpUsedAlignments()=0;
	virtual void clearAllUsedAlignments()=0;
	//std::vector<TAlignment*>::iterator lastAlignmentwithEndInWindow;
	//std::vector<TAlignment*>::iterator firstAlignmentwithPosOutsideWindow;

public:
	long start;
	long end; //end NOT included in window!
	long length;
	int chrNumber;
	std::string chrName;
	TSite* sites;
	bool sitesInitialized;
	int numReadsInWindow;
	double depth, fractionSitesNoData, fractionsitesDepthAtLeastTwo;
	double fractionRefIsN;
	long numSitesWithData;
	bool passedFilters;
	TBaseFrequencies baseFreq;

	~TWindow(){};

	//getters
	TBaseFrequencies getBaseFreq(){return baseFreq;};

	virtual TAlignment* swapUsedForEmptyAlignment(TAlignment* usedAlignment, const unsigned int & maxReadLength)=0;
	virtual void initSites(long newLength)=0;
	virtual void clear()=0;
	virtual void move(long Start, long End, int chrNumber)=0;
	virtual void jump(long Start, long End, int ChrNumber)=0;
	virtual void review()=0;
	virtual void printStacks()=0;
	virtual void fillSitesSubset(TSiteSubset* subset, const int & readUpToDepth)=0;
	virtual void fillSites(const int & readUpToDepth)=0;
	//virtual void addReferenceBaseToSites(BamTools::Fasta & reference)=0;
	virtual void addReferenceBaseToSites(TSiteSubset* subset)=0;
	virtual void applyMask(TBedReader* mask, bool inverseMasking)=0;
	virtual void maskCpG()=0;
	virtual void estimateBaseFrequencies()=0;
	virtual void calculateEmissionProbabilities()=0;
	//virtual void callMLEGenotype(TRecalibration* recalObject, TRandomGenerator & randomGenerator, gz::ogzstream & out, std::string & chr, bool printAll, bool printRef, bool isVCF, bool gVCF, bool noAltIfHomoRef)=0;
	virtual void printPileup(TRecalibration* recalObject, gz::ogzstream & out, bool printOnlySitesWithData)=0;
	virtual void printPileupToScreen(TRecalibration* recalObject)=0;
	virtual void calcDepth()=0;
	virtual void calcFracN()=0;
	virtual void calcDepthPerSite(long * siteDepth, size_t maxCov)=0;
	virtual void countDepthPerSite(TDistributionOfCounts & counts)=0;
	virtual void printDepthPerSite(gz::ogzstream & out, std::string & chr)=0;
	virtual void printMateInformationPerSite(TOutputFileZipped & out, std::string & chr)=0;
	virtual void countAlleles(long**** siteImbalance, const unsigned int & maxDepth)=0;
	virtual void applyDepthFilter(const size_t minDepth, const size_t maxDepth)=0;
	virtual void createDepthMask(size_t minDepth, size_t maxDepth, std::ofstream & outputMaskFile, std::string & chr)=0;

	//add sites to data structures
	virtual void addSitesToBQSR(TRecalibrationBQSREstimator & bqsr, TLog* logfile, TQualityMap & qualMap)=0;
	virtual void addSitesToBQSR(TRecalibrationBQSREstimator & bqsr, TSiteSubset* subset, TLog* logfile, TQualityMap & qualMap)=0;
	//virtual void addSitesToQualityTransformTable(TRecalibration* recalObject, std::vector<TQualityTransformTable*> & QTtables, TLog* logfile, TQualityMap & qualMap)=0;
	//virtual void addSitesToQualityTransformTable(TRecalibration* recalObject, TRecalibration* otherRecalObject, std::vector<TQualityTransformTable*> & QTtables, TLog* logfile, TQualityMap & qualMap)=0;
	virtual void addSitesToPMDTable(TPMDTables & pmdTables, TLog* logfile)=0;
	virtual void addSitesToThetaEstimator(TThetaEstimatorData* thetaDataContainer)=0;
	virtual void addSitesToThetaEstimator(TThetaEstimatorData* thetaDataContainer, TBedReader & region)=0;
	virtual void addToGLF(TGlfWriter & writer, bool printAll)=0;
	virtual void addToRecalibrationEM(TRecalibrationEMEstimator & recalObject, TQualityMap & qualMap)=0;
	virtual void addToRecalibrationEM(TRecalibrationEMEstimator & recalObject, TSiteSubset* subset, TQualityMap & qualMap)=0;

	//callers
	virtual void call(TCaller & caller, TRecalibration & recalObject, BamTools::Fasta & reference)=0;
	virtual void callKnwonAlleles(TCaller & caller, TRecalibration & recalObject, TSiteSubset & subset)=0;

	//other
	virtual void generatePSMCInput(TThetaEstimator & estimator, int & blockSize, double & confidence, std::ofstream & out, int & nCharOnLine)=0;
	double calcLogLikelihood();
};

#endif /* TWINDOW_H */

