/*
 * TVcfFile.h
 *
 *  Created on: Aug 8, 2011
 *      Author: wegmannd
 */

#ifndef TVCFFILE_H_
#define TVCFFILE_H_

#include <vector>
#include <map>
#include <iostream>
#include <fstream>
#include "TVcfParser.h"
#include <algorithm>
#include "../TLog.h"
#include "../gzstream.h"

typedef void (TVcfParser::*pt2Function)(TVcfLine &);
//---------------------------------------------------------------------------------------------------------
class TVcfFile_base{
public:
	std::istream* myStream;
	bool inputStreamOpend;
	std::ostream* myOutStream;
	bool outputStreamOpend;
	std::string fileFormat;
	//TVcfColumnNumbers cols;
	TVcfParser parser;
	unsigned int numCols;
	long currentLine;
	std::vector< pt2Function > usedParsers;
	bool automaticallyWriteVcf;
	TVcfLine tempLine;
	bool eof;
	double totalFileSize;
	std::string filename, outputFilename;

	//vector<TVcfFilter> filters;
	//bool applyFilters;

	std::vector<std::string> unknownHeader;

	TVcfFile_base(){currentLine=0; automaticallyWriteVcf=false;eof=false;numCols=-1;totalFileSize=-1;myOutStream=NULL; myStream=NULL; inputStreamOpend=false; outputStreamOpend=false;};
	TVcfFile_base(std::string & filename, bool zipped);
	virtual ~TVcfFile_base(){
		if(inputStreamOpend) delete myStream;
		if(outputStreamOpend) delete myOutStream;
	};
	void enableAutomaticWriting(){
		if(!outputStreamOpend)
			throw "Can not automatically write VCF: no output stream has been opened!";
		automaticallyWriteVcf = true;
	};
	void openStream(std::string & filename, bool zipped);
	void openOutputStream(std::string & filename, bool zipped);
	void setOutStream(std::ostream & is);

	//which parsers to use?
	void enablePositionParsing(){usedParsers.push_back(&TVcfParser::parsePosition);};
	void enableVariantQualityParsing(){usedParsers.push_back(&TVcfParser::parseQuality);};
	void enableVariantParsing(){usedParsers.push_back(&TVcfParser::parseVariant);};
	void enableInfoParsing(){usedParsers.push_back(&TVcfParser::parseInfo);};
	void enableFormatParsing(){usedParsers.push_back(&TVcfParser::parseFormat);};
	void enableSampleParsing(){usedParsers.push_back(&TVcfParser::parseSamples);};

	//retrieve info
	GTLikelihoods _genotypeLikelihoods(TVcfLine* line, unsigned int sample);
	GTLikelihoods _genotypeLikelihoodsPhred(TVcfLine* line, unsigned int sample);
	void fillGenotypeLiklihoods(TVcfLine* line, unsigned int sample, float* gtl);
	void fillPhrdScore(TVcfLine* line, unsigned int sample, uint8_t* gtl);
	int sampleNumber(std::string & Name);
	int numSamples();
	std::string sampleName(unsigned int num);
	bool sampleIsMissing(TVcfLine* line, unsigned int sample);
	bool sampleHasUnknownGenotype(TVcfLine* line, unsigned int sample);
	virtual std::string fieldContentAsString(std::string tag, TVcfLine* line, unsigned int sample);
	virtual int fieldContentAsInt(std::string tag, TVcfLine* line, unsigned int sample);
	virtual int depthAsIntNoCheckForMissingSample(std::string tag, TVcfLine* line, unsigned int sample);

	//modify
	void setSampleMissing(TVcfLine* line, unsigned int sample);
	void setSampleHasUndefinedGenotype(TVcfLine* line, unsigned int sample);
	void updateField(TVcfLine* line, std::string & tag, std::string & Data, unsigned int sample);

	//void addFilter(my_string filter);
	//void filterSamples();
	//void printFilters();

	void parseHeaderVCF_4_0();
	void writeHeaderVCF_4_0();
	void addNewHeaderLine(std::string headerLine);
	bool readLine();
	void addFormat(std::string Line){parser.addFormat(Line);};

	//modify header and columns
	void updateInfo(TVcfLine* line, std::string Id, std::string Data);
	void addToInfo(TVcfLine* line, std::string Id, std::string Data);
	void updatePL(TVcfLine* line, std::string Data, int S);
};

class TVcfFileSingleLine:public TVcfFile_base{
public:
	bool written;

	TVcfFileSingleLine(){written=true;};
	TVcfFileSingleLine(std::string & filename, bool zipped);
	virtual ~TVcfFileSingleLine();
	void writeLine();
	bool next();
	//call specific parsers
	void parseInfo(){ parser.parseSamples(tempLine); };
	void parseFormat(){ parser.parseFormat(tempLine); };
	void parseSamples(){ parser.parseSamples(tempLine); };
	//other stuff
	TVcfLine* pointerToVcfLine(){return &tempLine;};
	void updateInfo(std::string Id, std::string Data);
	void addToInfo(std::string Id, std::string Data);
	void updatePL(std::string Data, unsigned int sample);
	virtual std::string fieldContentAsString(std::string tag, unsigned int sample);
	virtual int fieldContentAsInt(std::string tag, unsigned int sample);
	virtual int depthAsIntNoCheckForMissingSample(std::string tag, unsigned int sample);
	GTLikelihoods genotypeLikelihoods(unsigned int sample);
	GTLikelihoods genotypeLikelihoodsPhred(unsigned int sample);
	void fillGenotypeLikelihoods(unsigned int sample, float* gtl);
	void fillPhredScore(unsigned int sample, uint8_t* gtl);
	//variant info
	long position();
	std::string chr();
	bool variantQualityIsMissing();
	double variantQuality();
	int getNumAlleles();
	char getRefAllele();
	char getFirstAltAllele();
	char getAllele(int num);
	//sampel info
	void setSampleMissing(unsigned int sample);
	void setSampleHasUndefinedGenotype(unsigned int sample);
	void updateField(std::string tag, std::string & Data, unsigned int sample);
	bool sampleIsMissing(unsigned int sample);
	bool sampleHasUndefinedGenotype(unsigned int sample);
	bool sampleIsHomoRef(unsigned int sample);
	bool sampleIsHeteroRefNonref(unsigned int sample);
	char getFirstAlleleOfSample(unsigned int num);
	char getSecondAlleleOfSample(unsigned int num);
	short sampleGenotype(const unsigned int & num);
	float sampleGenotypeQuality(unsigned int sample);
	double sampleDepth(unsigned int sample);
	// int sampleDepth(unsigned int sample);
	bool formatColExists(std::string tag){ return parser.formatColExists(tag, tempLine); };
	std::string getSampleContentAt(std::string tag, unsigned int sample){
		return parser.sampleContentAt(tempLine, tag, sample);
	}
};


#endif /* TVCFFILE_H_ */
