/*
 * TVcfDiagnostics.cpp
 *
 *  Created on: Jun 15, 2011
 *      Author: wegmannd
 */
#include <algorithm>
#include <typeinfo>
#include <sstream>
#include "../gzstream.h"
#include "TVcfDiagnostics.h"

//---------------------------------------------------
//TAnnotator
//---------------------------------------------------
VcfDiagnostics::VcfDiagnostics(TParameters & Params, TLog* Logfile){
	logfile = Logfile;
	verbose = Params.parameterExists("verbose");
	randomGeneratorInitialized = false;
	randomGenerator = NULL;
	chr=-1;

	//open vcf file
	openVCF(Params.getParameterString("vcf"), vcfFile);

	//outputname
	outname = Params.getParameterStringWithDefault("out", "");
	if(outname == ""){
		//guess from filename
		outname = vcfFile.filename;
		outname = extractBeforeLast(outname, ".");
		if(isZipped)
			//if zipped there is extra .gz
			outname = extractBeforeLast(outname, ".");

	}
	logfile->list("Writing output files with prefix '" + outname + "'.");
}

//open input stream
void VcfDiagnostics::openVCF(std::string filename, TVcfFile_base & vcfFile){
	//open vcf file
	if(filename.find(".gz") == std::string::npos){
		logfile->list("Reading vcf from file '" + filename + "'.");
		vcfFile.openStream(filename, false);
		isZipped = false;
	} else {
		logfile->list("Reading vcf from gzipped file '" + filename + "'.");
		vcfFile.openStream(filename, true);
		isZipped = true;
	}
}

void VcfDiagnostics::initializeRandomGenerator(){
	if(!randomGeneratorInitialized){
		randomGenerator=new TRandomGenerator();
		logfile->list("Random generator initialized with seed " + toString(randomGenerator->usedSeed));
		randomGeneratorInitialized=true;
	}
}

int VcfDiagnostics::baseToNumber(char base, std::string & marker){
	if(base == 'A') return 0;
	else if(base == 'C') return 1;
	else if(base == 'G') return 2;
	else if (base == 'T') return 3;
	else throw "unknown base " + toString(base) + " at marker " + marker;
}

void VcfDiagnostics::vcfToInvariantBed(){
	//open vcf file
	logfile->list("Writing sites that are invariant across individuals to bed:");
	//enable parsers
	vcfFile.enablePositionParsing();
	vcfFile.enableFormatParsing();
	vcfFile.enableSampleParsing();
	vcfFile.enableVariantParsing();

	//open output
	logfile->list("Writing files to '" + outname + ".bed.gz'");
	gz::ogzstream bedFile((outname + (std::string) ".bed.gz").c_str());

	//parse vcf file
	std::map<char, int> bases = {{'A', 1}, {'C', 1}, {'G', 1}, {'T', 1}};
	int counter = 0;
	int curStartRegion = -1;
	bool previousStartIsVariant = false;
	std::string curChr;
	bool updateStart = true;
	long lastPosition;
	while(vcfFile.next()){
		++counter;
		if(updateStart){
			curStartRegion = vcfFile.position();
			curChr = vcfFile.chr();
			std::cout << "updated start to " << curStartRegion << " on chr " << curChr << std::endl;
			updateStart = false;
		}
		if(bases.find(vcfFile.getRefAllele()) == bases.end()) {//!= 'A' && vcfFile.getRefAllele() != 'C' && vcfFile.getRefAllele() != 'G' && vcfFile.getRefAllele() != 'T'
			continue; //ignore indels
		}

		if(curChr != vcfFile.chr()){
			std::cout << "found that chr changed at line " << counter << std::endl;
			//is previous site invariant? -> write to file
			if(!previousStartIsVariant){
				bedFile << curChr << "\t" << curStartRegion - 1 << "\t" << lastPosition << std::endl;
				std::cout << "wrote line to file. counter is " << counter << " and coordinates are " << curChr << ":" << curStartRegion << "-" << lastPosition << std::endl;
			} else {
				std::cout << "chr changed but not writing because " << vcfFile.position() << " is not larger than " << curStartRegion << std::endl;
			}
			//update start directly, don't just set to true
			curStartRegion = vcfFile.position();
			curChr = vcfFile.chr();
		}

		int indCounter = 0;
		char allele = vcfFile.getFirstAlleleOfSample(0);
		for(int ind=0; ind<vcfFile.numSamples(); ++ind){
			++indCounter;
			if(vcfFile.getFirstAlleleOfSample(ind) != allele || vcfFile.getSecondAlleleOfSample(ind) != allele){
				std::cout << "found variant site at line " << counter << std::endl;
				//there was a variant site, is previous site invariant? -> write to file
				if(previousStartIsVariant == false && counter != 1){
					bedFile << vcfFile.chr() << "\t" << curStartRegion - 1 << "\t" << vcfFile.position() - 1 << std::endl;
					std::cout << "previousStartIsVAriant=" << previousStartIsVariant << " wrote line to file. coordinates were " << curStartRegion - 1 << "-" << vcfFile.position() << std::endl;
				}
				updateStart = true;
				previousStartIsVariant = true;
				std::cout << "set previousStartIsVariant to true" << std::endl;
				break;
			}
			else {
				previousStartIsVariant = false;
				std::cout << "found line to be invariant at counter " << counter << " and at position " << vcfFile.position()<< std::endl;
			}
		}


//		if(indCounter == vcfFile.numSamples()){
//			//bed is 0-based
//			bedFile << vcfFile.chr() << "\t" << vcfFile.position() - 1 << "\t" << vcfFile.position() << std::endl;
//		}

		//in case of chr change, this is needed to write last region of previous chr
		lastPosition = vcfFile.position();

	}

	//write last region to file
	if(!previousStartIsVariant){
		bedFile << vcfFile.chr() << "\t" << curStartRegion - 1 << "\t" << vcfFile.position() << std::endl;
		std::cout << "wrote last region to file. curStartRegion " << curStartRegion << std::endl;
	}
	bedFile.close();
}

void VcfDiagnostics::vcfToBeagle(){
	//enable parsers
	vcfFile.enablePositionParsing();
	vcfFile.enableFormatParsing();
	vcfFile.enableVariantParsing(); //has to come before sample parsing!!
	vcfFile.enableSampleParsing();
	vcfFile.enableInfoParsing();

	//open output files:
	gz::ogzstream beagleFile((outname + (std::string) ".beagle.gz").c_str());

	//other variables
	int vcfLines = 0;

	//header string
	std::string header = ""; header += "marker\tallele1\tallele2";
	for(int i=0; i<vcfFile.numSamples(); ++i){
		for(int r=0; r<3; ++r){
			header += "\t" + vcfFile.sampleName(i);
		}
	}
	header += "\n";

	beagleFile << header;

	while(vcfFile.next()){
		//report progress
		++vcfLines;
		std::cout << "vcfLines " << vcfLines << std::endl;
		if(verbose && fmod(vcfLines, 100000000) == 0)
			std::cerr << " - Progress: Lines read from vcfFile = " << vcfLines << std::endl;

		//write line
		std::string marker = ""; marker += vcfFile.chr();marker += "_"; marker += toString(vcfFile.position());
		beagleFile << marker;
		beagleFile << "\t" << baseToNumber(vcfFile.getRefAllele(), marker) << "\t" << baseToNumber(vcfFile.getFirstAltAllele(), marker);
		for(int i=0; i<vcfFile.numSamples(); ++i){
			if(vcfFile.sampleIsMissing(i)){
				beagleFile << "\t0.333\t0.333\t0.333";
			} else {
//				std::cout << "i " << i << " ll " << vcfFile.genotypeLikelihoods(i).AA << " phred " << vcfFile.genotypeLikelihoodsPhred(i).AA << std::endl;
				beagleFile << "\t" << vcfFile.genotypeLikelihoodsPhred(i).AA
					<< "\t" << vcfFile.genotypeLikelihoodsPhred(i).AB
					<< "\t" << vcfFile.genotypeLikelihoodsPhred(i).BB;
			}
		}
		beagleFile << "\n";
	}

	beagleFile.close();

}

int VcfDiagnostics::findLastPassedFilterIndex(int obsValue, std::vector<int> & filtersAscendingOrder){
	int lastPassedFilterIndex = 0;
	for(unsigned int i=0; i<filtersAscendingOrder.size(); ++i){
		if(obsValue < filtersAscendingOrder[i])
			break;
		else
			lastPassedFilterIndex = i;
	}
	return lastPassedFilterIndex;
}

void VcfDiagnostics::assessAllelicImbalance(TParameters & Params){
	//enable parsers
	vcfFile.enablePositionParsing();
	vcfFile.enableFormatParsing();
	vcfFile.enableSampleParsing();
	vcfFile.enableVariantParsing();

	//output
	logfile->list("Writing files to '" + outname + "_allelicDepth.txt'");

	//limit input?
	int maxDP = Params.getParameterIntWithDefault("maxDepth", 100);
	logfile->list("Ignoring sites with depth larger than " + toString(maxDP) + ".");

	long inputLines = Params.getParameterLongWithDefault("inputLines", -1);
	if(inputLines <= 0){
		logfile->list("Reading whole vcf.");
	} else
		logfile->list("Limiting input to " + toString(inputLines) + " lines.");

	//initialize tables
	logfile->startIndent("Initializing count tables:");
	std::string qualityString = Params.getParameterStringWithDefault("qualities", "0,10,20,30,40,50");
	std::vector<int> qualities;
	fillVectorFromString(qualityString, qualities, ',');

	std::vector<TCountTable*> countTables;
	for(unsigned int i=0; i<qualities.size(); ++i){
		countTables.emplace_back(new TCountTable(maxDP, maxDP, outname + "_qual" + toString(qualities[i]) + "_allelicDepth.txt", logfile));
	}
	logfile->endIndent();

	//temp variables
	int counter = 0;
	int numHet = 0;

	logfile->startIndent("Parsing vcf file:");
	while(vcfFile.next()){
		++counter;

		//get allelic depth at hetero sites across all samples. One table per quality filter.
		for(int i=0; i < vcfFile.numSamples(); ++i){
			if(!vcfFile.sampleIsMissing(i) && vcfFile.sampleIsHeteroRefNonref(i)){
				++numHet;

				//which position in table does site correspond to?
				std::vector<std::string> tmp;
				std::string tag="AD";
				fillVectorFromString(vcfFile.getSampleContentAt(tag, i), tmp, ',');
				int numRef = stringToInt(tmp[0]);
				int numAlt = stringToInt(tmp[1]);
				if(numRef == 0 || numAlt == 0)
					throw "Call at position " + toString(vcfFile.position()) + " is heterozygous but reference or alternative allelic depth is 0!";
				if(vcfFile.depthAsIntNoCheckForMissingSample("DP", i) > maxDP){
					logfile->warning("DP is " + toString(vcfFile.depthAsIntNoCheckForMissingSample("DP", i)) + " at pos " + toString(vcfFile.position()) + ". This site will be ignored.");
					continue;
				}

				//add count to correct table
				int quality = stringToInt(vcfFile.getSampleContentAt("GQ", i));
				int index = findLastPassedFilterIndex(quality, qualities);
				for(int i=0; i<(index+1); ++i){
					++(countTables.at(i))->table[numRef][numAlt];
				}
			}
		}
		if(verbose && counter % 1000000 == 0)
			logfile->list("read " + toString(counter) + " lines, " + toString(numHet) + " heterozygous sites were found.");
		if(inputLines != -1 && counter > inputLines){
			logfile->list("reached input limit!");
			break;
		}
	}

	logfile->endIndent("done!");

	std::string description = "ref_depth/alt_depth";
	std::string rowPrefix = "ref_";
	std::string colPrefix = "alt_";


	//write tables
	for(unsigned int i=0; i<countTables.size(); ++i){
		countTables[i]->writeTable(description, rowPrefix, colPrefix);
		delete countTables[i];
	}

	//clean up
//	for(int i=0; i<countTables.size(); ++i){
//		delete countTables.at(i);
//	}

}
/*
void VcfDiagnostics::filterAllelicImbalance(){
	//open vcf file
	if(verbose) std::cerr << " - Filtering sites with allelic imbalance:" << std::endl;

	double pValFilter = params->getParameterDoubleWithDefault("pValFilter", 0.001);

	openVCF(vcfFile);

	//enable parsers
	vcfFile.enablePositionParsing();
	vcfFile.enableFormatParsing();
	vcfFile.enableSampleParsing();
	vcfFile.enableVariantParsing();
	vcfFile.enableWriting();

	//prepare output vcf
	prepareVcfOutput(vcfFile);
	vcfFile.writeHeaderVCF_4_0();

	//how to filter?
	int counter = 0;
	std::string tag="AD";
	int numRef; int numAlt; int tot;
	int numHet;
	std::vector<int> tmp;
	double logHalf = log(0.5);
	double cumul;
	int i;
	long numFiltered = 0;
	long numPassedFilter = 0;

	//open log file for sites
	std::string filename = params->getParameterStringWithDefault("infoOut", "ImbalanceFilter_siteInfo.txt");
	std::ofstream sitesInfoOut(filename.c_str());
	if(!sitesInfoOut)
		throw "Failed to open file '" + filename + " for writing!";
	sitesInfoOut << "chr\tpos\tnumHet\tnumRef\tnumAlt\taltFrac\tpVal\tstatus\n";

	while(vcfFile.next()){
		++counter;

		//count allelic depth across all samples
		numRef = 0; numAlt = 0;
		numHet = 0;
		for(int i=0; i < vcfFile.numSamples(); ++i){
			if(!vcfFile.sampleIsMissing(i) && vcfFile.sampleIsHeteroRefNonref(i)){
				fillVectorFromString(vcfFile.getSampleContentAt(tag, i), tmp, ',');
				numRef += tmp[0]; numAlt += tmp[1];
				++numHet;
			}
		}

		//write info
		sitesInfoOut << vcfFile.chr() << "\t" << vcfFile.position() << "\t" << numHet;

		//calculate p-value for sampling
		if(numHet > 0){
			sitesInfoOut << "\t" << numRef << "\t" << numAlt << "\t" << (double) numAlt / (double)(numAlt+numRef);
			cumul = 0.0;
			tot = numAlt + numRef;
			if(numAlt < numRef){
				for(i = 0; i <= numAlt; ++i)
					cumul += exp(randomGenerator->binomCoeffLn(tot, i) + logHalf*tot);
			} else {
				for(i = numAlt; i <= tot; ++i)
					cumul += exp(randomGenerator->binomCoeffLn(tot, i) + logHalf*tot);
			}

			//now filter
			if(cumul < pValFilter){
				++numFiltered;
				vcfFile.written = true;
				sitesInfoOut << "\t" << cumul << "\tfiltered\n";
			} else {
				vcfFile.writeLine();
				++numPassedFilter;
				sitesInfoOut << "\t" << cumul << "\tpassed\n";
			}
		} else {
			++numPassedFilter;
			sitesInfoOut << "\t-\t-\t-\t-\tpassed\n";
		}

		if(verbose && counter % 1000 == 0)	std::cerr << "    - read " << counter << " lines, " << numPassedFilter << " passed, " << numFiltered << " were filtered out." << std::endl;
	}

	//clean up
	sitesInfoOut.close();
}

*/


void VcfDiagnostics::fixIntAsFloat(){
	//open vcf file
	logfile->list("Fixing integers that are printed as floats:");

	//enable parsers
	vcfFile.enablePositionParsing();
	vcfFile.enableFormatParsing();
	vcfFile.enableSampleParsing();
	vcfFile.enableVariantParsing();

	//open output file
	std::string filename = outname + (std::string) "_fixed.vcf.gz";
	gz::ogzstream out(filename.c_str());
	if(!out) throw "Failed to open outputfile '" + filename + "'!";
	vcfFile.setOutStream(out);
	vcfFile.writeHeaderVCF_4_0();

	//tmp vars
	int counter = 0;
	std::vector<int> vec;

	//parse VCF

	logfile->startIndent("Parsing vcf file:");
	while(vcfFile.next()){
		++counter;

		//fix GP field
		std::string gp = vcfFile.fieldContentAsString("GP", 0);
		fillVectorFromString(gp, vec, ',');
		gp = std::to_string(vec[0]) + ',' + std::to_string(vec[1]) + ',' + std::to_string(vec[2]);
		vcfFile.updateField("GP", gp, 0);

		//write output
		vcfFile.writeLine();

		//report progress
		if(verbose && counter % 1000000 == 0)
			logfile->list("read " + toString(counter) + " lines.");
	}
	logfile->endIndent();
};

