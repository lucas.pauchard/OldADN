/*
 * estimHet.cpp
 *
 *  Created on: Feb 19, 2015
 *      Author: wegmannd
 */

#include "TMain.h"

//---------------------------------------------------------------------------
//Main function
//---------------------------------------------------------------------------
int main(int argc, char* argv[]){
	TMain main("Atlas", "0.9", "https://bitbucket.org/wegmannlab/atlas");
	return main.run(argc, argv);
};

