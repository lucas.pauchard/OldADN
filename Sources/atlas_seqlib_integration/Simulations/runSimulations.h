/*
 * runSimulations.h
 *
 *  Created on: May 19, 2017
 *      Author: wegmannd
 */

#ifndef RUNSIMULATIONS_H_
#define RUNSIMULATIONS_H_

#include "TSimulator.h"
#include "../TParameters.h"
#include "../TLog.h"

void runSimulations(TParameters & params, TLog* logfile);



#endif /* RUNSIMULATIONS_H_ */
