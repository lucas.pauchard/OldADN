/* 
 * File:   TAlignment.h
 * Author: Lucas Pauchard
 *
 * Created on 18. avril 2019, 09:35
 */

#ifndef TALIGNMENT_H
#define TALIGNMENT_H

#include "../stringFunctions.h"
#include "../TBase.h"
#include "../TPostMortemDamage.h"
#include "../Recalibration/TRecalibration.h"
#include "../bamtools/api/BamAlignment.h"
#include "../bamtools/utils/bamtools_fasta.h"
#include "../bamtools/api/BamWriter.h"
#include "../TQualityMap.h"
#include "../QualityTables.h"

//-----------------------------------------------------
//TAlignment
//-----------------------------------------------------

class TAlignment{
protected:
	//details
	bool empty;
	bool recalibrated;

	//data
	unsigned int maxSize;
	int length;

	//booleans
	bool parsed;
	bool changed;

	//per base data
	TBase* bases;
	int* softClippedLength;
	char** softClippedBase;
	char** softClippedQuality;
	int* qualityOriginal; //Note: quality is char as int: quality = (int) bam.quality
	int numInsertions;
	int numDeletions;
	uint8_t softClippedEntry; //0 means start, 1 means end of read

	//reference
	bool hasReference;
	std::string referenceSequence;

public:
	bool storageInitialized;
	BamTools::BamAlignment bamAlignment;

	/*TAlignment() = 0;
	TAlignment(unsigned int MaxSize) = 0;
	TAlignment(const TAlignment & Alignment) = 0;*/

	~TAlignment(){};

	int getPosition(){ return position; };
	int getParsedLength(){ return length; };
	virtual int getBamAlignmentLength()=0;
	std::string name(){ return alignmentName; };
	virtual int32_t getInsertSize()=0;

	//functions to write / print alignment
	virtual void setToSingleEnd() = 0;
	virtual void setIsProperPair(const bool & ok) = 0;
	virtual void save(BamTools::BamWriter & bamWriter, TGenotypeMap & genoMap, int & minQualForPrinting, int & maxQualForPrinting, TQualityMap & qualMap) = 0;
	virtual void print(TGenotypeMap & genoMap, TQualityMap & qualMap) = 0;
	virtual void setAlignmentHasChanged(){ changed = true; };

	//accessed by alignmentParser
	virtual void filterForBaseQuality(int & minQual, int & maxQual) = 0;
	virtual void clear() = 0;
	virtual void parse(TGenotypeMap & genoMap, TQualityMap & qualityMap) = 0;

	//accessed by TGenome
	int readGroupId;
	bool isReverseStrand;
	bool isPaired;
	bool isProperPair;
	int mappingQuality;
	bool passedFilters;
	bool isSecondMate;
	int chrNumber;
	int32_t position;
	int32_t lastPositionPlusOne;
	int32_t lastAlignedPositionWithRespectToRef;
	int32_t lastAlignedPos;
	std::string alignmentName;

	//TODO: move these functions to TGenome
	virtual void fillReadGroupInfo(int & readGroupID) = 0;
	virtual void binQualityScores(TQualityMap & qualityMap) = 0;
	virtual void updateOptionalSamField(std::string tag, float value) = 0;
	virtual void updateOptionalSamField(std::string tag, std::string value) = 0;
	virtual void downsampleAlignment(double& fraction, TRandomGenerator& randomGenerator, TQualityMap & qualMap) = 0;

	virtual void addToPMDTables(TPMDTables & pmdTables, TGenotypeMap & genoMap) = 0;
	virtual void recalibrateWithPMD(TRecalibration* recalObject, TQualityMap & qualMap) = 0;
	virtual double calculatePMDS(double & pi, TPMD* pmdObjects) = 0;
	virtual void assessSoftClipping(int & S_left, int & middle, int & S_right, std::string & S_string_left, std::string & S_string_middle, std::string & S_qualities_middle, std::string & S_string_right, TGenotypeMap & genoMap) = 0;
	virtual void removeSoftClippedBases(int & S_left, int & middle, int & S_right, std::string & S_string_left, std::string & S_string_middle, std::string & S_qualities_middle, std::string & S_string_right, TGenotypeMap & genoMap) = 0;
	virtual int measureOverlap() = 0;
	virtual void addToQualityTable(TQualityTable & qualTable, TQualityMap & qualMap) = 0;

	friend class TAlignmentParser;
	friend class TAlignmentParserBamTools;
	friend class TAlignmentParserSeqLib;
	friend class TAlignmentBamTools;
	friend class TAlignmentSeqLib;
	friend class TWindow;
	friend class TWindowBamTools;
	friend class TWindowSeqLib;
};

#endif /* TALIGNMENT_H */

