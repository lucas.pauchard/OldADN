/* 
 * File:   TAlignmentParser.h
 * Author: LP-Portable
 *
 * Created on 18. avril 2019, 10:35
 */
#ifndef TALIGNMENTPARSER_H_
#define TALIGNMENTPARSER_H_


#include "../bamtools/api/BamReader.h"
#include "../bamtools/api/BamWriter.h"
#include "../bamtools/api/SamSequenceDictionary.h"
#include "../bamtools/utils/bamtools_fasta.h"
#include "../TGenotypeMap.h"
#include "../TReadGroups.h"
#include "../TLog.h"
#include "../Recalibration/TRecalibration.h"
#include "../TPostMortemDamage.h"
#include "TAlignment.h"
#include "TAlignmentBamTools.h"
#include "TAlignmentSeqLib.h"
#include "../Window/TWindow.h"
#include "../Window/TWindowBamTools.h"
#include "../Window/TWindowSeqLib.h"
#include "../TBed.h"
#include "../TBedReader.h"
#include <vector>
#include <iostream>

//-----------------------------------------------------
//TFastaBuffer
//-----------------------------------------------------
//a buffer class to speed up adding the reference sequence to each read
//This class makes use of the fact that bam files are sorted, hence the buffer can always start at the current position

class TFastaBuffer{
private:
	BamTools::Fasta* reference;
	int bufferSize;
	std::string referenceSequence;

	int curChr;
	long curStart, curEnd;

	void moveTo(const int & chr, const int32_t & pos);

public:
	TFastaBuffer(BamTools::Fasta* Reference);
	~TFastaBuffer(){};
	void fill(const int & chr, const int32_t & start, const int32_t end, std::string & ref);
	int getCurChr(){return curChr;};
	long getCurStart(){return curStart;};
	long getCurEnd(){return curEnd;};
};

//-----------------------------------------------------
//TAlignmentParser
//-----------------------------------------------------
class TAlignmentParser{
protected:

	//TReadGroups* readGroupTable;

	TLog* logfile;
	bool _keepDuplicates;
	bool _keepImproperPairs;
	bool _parse;
	int previousAlignmentPos;
	int previousAlignmentChr;
	TAlignment* oldAlignment;
	bool oldAlignmentInitialized;
	bool oldAlignmentMustBeConsidered;

	//counters
	int totalNumberAlignmentsRead;
	int64_t sizeOfBamFile;

	//read trimming
	bool trimReads;
	int trimmingLength3Prime;
	int trimmingLength5Prime;

	//iterators
 	int chrNumber;
 	long chrLength;

	//window params
	int windowSize;
	int numWindowsOnChr;
	int windowNumber;

	unsigned int maxReadLength;
	double maxMissing;
	double maxRefN;

	//masks
	TBedReader* mask;

	//filters
	bool applyQualityFilter;
	size_t readUpToDepth, minDepth, maxDepth;
	int minPhredInt, maxPhredInt;
	bool applyFragmentLengthFilter;
	//bool keepOnlyFwd, keepOnlyRev;
	bool useStrand[2];
	bool useMate[2];

	

	//limit chr and windows
	long limitWindows;
	int skipWindows;
	int limitChr;
	bool* useChromosome;

public:
	//alignment: goal is to make this private!
	int curReadGroupID;
	int minQualForPrinting, maxQualForPrinting;
	int minQual, maxQual;
	bool doMasking, considerRegions;
	bool doCpGMasking;
	bool applyDepthFilter;
	bool windowsPredefined;
	TBed* predefinedWindows;
	bool sitesProvided;
	TSiteSubset* subset = NULL;
 	TReadGroups readGroups;

	//maps
	TGenotypeMap genoMap;
	TQualityMap qualMap;

	//BAM file
	std::string filename;
        BamTools::BamAlignment bamAlignment;
	BamTools::BamReader bamReader;
	BamTools::BamRegion bamRegion;
 	BamTools::SamHeader bamHeader;
 	BamTools::SamSequenceIterator chrIterator;

 	//reference
	BamTools::Fasta* fastaReference;
	bool hasReference;
	bool chrChanged;
	TFastaBuffer* fastaBuffer;
        
 	//recalibration
	TRecalibration* recalObject;
	bool doRecalibration;
	bool recalObjectInitialized;

	//PMD
	bool hasPMD;
	TPMD* pmdObjects;

	//construction
	/*TAlignmentParser();
	TAlignmentParser(int MaxReadLength, TParameters & params, TLog* Logfile);*/
	~TAlignmentParser(){};
	virtual void init(int MaxReadLength, TParameters & params, TLog* Logfile)=0;

	//getters
	bool qualitiesScoresAreRecalibrated(){ return recalObject->recalibrationChangesQualities(); };
	int numReadGroups(){ return readGroups.size(); };
	std::string recalibrationType(){ return recalObject->type(); };
	int getWindowSize(){return windowSize;};
	int getMaxPhredInt(){return maxPhredInt;};
	int getNumAlignmentsRead(){ return totalNumberAlignmentsRead; };
	virtual double getPositionInFile()=0;

	//setters
	virtual void setQualityFilters(int minQual, int maxQual)=0;
	virtual void setQualityRangeForPrinting(int minQual, int maxQual)=0;
	virtual void setReadTrimming(int trim3Prime, int trim5Prime)=0;
	virtual void setApplyFragmentLengthFilter(bool filterYesNo)=0;

	void keepDuplicates(){_keepDuplicates = true;};
	void keepImproperPairs(){_keepImproperPairs = true;};
	void setParsingToTrue(){_parse = true;};
	virtual void fillReferenceSequence(TFastaBuffer* fastaBuffer, TAlignment & alignment)=0;
	virtual std::string chrNumberToName(int chrNumber)=0;
	virtual int chrNumberToLength(int chrNumber)=0;
	virtual long calcReferenceLength()=0;

	//blacklist
	virtual void setUpdateBlacklistToTrue()=0;
	virtual void setWriteBlacklistToFileToTrue()=0;
	virtual void addToBlacklist(TAlignment & alignment, const std::string & errorMessage)=0;
	virtual void addToBlacklist(BamTools::BamAlignment & alignment, const std::string & errorMessage)=0;
	virtual void addToBlacklist(std::string & alignmentName, const std::string & errorMessage)=0;
	virtual void removeFromBlacklist(TAlignment & alignment, const std::string & errorMessage)=0;
	virtual bool isInBlacklist(std::string & alignmentName)=0;

	//functions to read and _parse
	//virtual void checkAndFillAlingment(BamTools::BamAlignment& bamAlignment, TAlignment & alignment);
	virtual void addReference(BamTools::Fasta* reference)=0;
	virtual void recalibrate(TAlignment & alignment)=0;

	//reading data requires windows
	virtual bool readDataInNextWindow(TWindow & window)=0;

	//reading data only requires alignments
	virtual bool readNextAlignment(TAlignment & alignment)=0; //to be used to go through bam file alignment by alignment
	virtual bool readNextAlignmentWithBlacklist(TAlignment & alignment)=0;

	//qualityTransformation
	//virtual void initializeRecalibrationForQualityTransformation(TParameters & params);
	virtual void addSitesToQualityTransformTable(TAlignment & alignment, TQualityTransformTables & QTtables)=0;
	virtual void addSitesToQualityTransformTable(TAlignment & alignment, TRecalibration* otherRecalObject, TQualityTransformTables & QTtables)=0;
	virtual void mergeAlignedBasesBamReads(TAlignment* fwdAlignment, TAlignment* revAlignment, bool adaptQuality)=0;
};

//-----------------------------------------------------
// TBamProgressReporter
//-----------------------------------------------------
class TBamProgressReporter{
private:
	timeval start, end;
	TAlignmentParser* parser;
	TLog* logfile;
	int progressFrequency;
	int lastProgressPrinted;

	void _init(int Frequency, TAlignmentParser* Parser, TLog* Logfile);
	std::string _getRunTime();
	void _printProgress();

public:
	TBamProgressReporter(int Frequency, TAlignmentParser* Parser, TLog* Logfile);
	TBamProgressReporter(TAlignmentParser* Parser, TLog* Logfile);

	void printProgress();
	void printEnd();
};


#endif /* TALIGNMENTPARSER_H_ */
