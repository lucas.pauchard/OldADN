/*
 * TAlignmentSeqLib.h
 *
 *  Created on: Apr 19, 2018
 *      Author: wegmannd
 */

#ifndef TALIGNMENT_H_SEQLIB
#define TALIGNMENT_H_SEQLIB

#include "TAlignment.h"

//-----------------------------------------------------
//TAlignmentBamTools
//-----------------------------------------------------

class TAlignmentSeqLib : public TAlignment{
private:
	/*//details
	bool empty;
	bool recalibrated;

	//data
	unsigned int maxSize;
	int length;

	//booleans
	bool parsed;
	bool changed;

	//per base data
	TBase* bases;
	int* softClippedLength;
	char** softClippedBase;
	char** softClippedQuality;
	int* qualityOriginal; //Note: quality is char as int: quality = (int) bam.quality
	int numInsertions;
	int numDeletions;
	uint8_t softClippedEntry; //0 means start, 1 means end of read

	//reference
	bool hasReference;
	std::string referenceSequence;*/

	//functions
	void initStorage();
	void freeStorage();

	//functions to read and parse
	void setDistancesFromEnds();
	void parseBasesQualities(TGenotypeMap & genoMap, TQualityMap & qualityMap);
	void fillContext(TGenotypeMap & genoMap);
	void fillPmdProbabilities(TPMD* pmdObjects);

	//functions to modify data
	void filterForPrintingBaseQuality(std::string & qual, int & minQualForPrinting, int & maxQualForPrinting);
	void trimRead(int & trimmingLength3Prime, int & trimmingLength5Prime);
	void setReadTrimming(int trim3Prime, int trim5Prime);

public:
	//bool storageInitialized;

	TAlignmentSeqLib();
	TAlignmentSeqLib(unsigned int MaxSize);
	TAlignmentSeqLib(const TAlignment & Alignment);

	~TAlignmentSeqLib(){
		freeStorage();
	};

	void fill(BamTools::BamAlignment & bamAlignment, int ReadGroupId);
	void setReferenceAdded();
	int getBamAlignmentLength(){ return bamAlignment.Length; };
	int32_t getInsertSize(){ return bamAlignment.InsertSize; };

	//functions to write / print alignment
	void setToSingleEnd();
	void setIsProperPair(const bool & ok);
	void save(BamTools::BamWriter & bamWriter, TGenotypeMap & genoMap, int & minQualForPrinting, int & maxQualForPrinting, TQualityMap & qualMap);
	void print(TGenotypeMap & genoMap, TQualityMap & qualMap);
	

	//accessed by alignmentParser
	void filterForBaseQuality(int & minQual, int & maxQual);
	void clear();
	void parse(TGenotypeMap & genoMap, TQualityMap & qualityMap);

	//accessed by TGenome
	/*int readGroupId;
	bool isReverseStrand;
	bool isPaired;
	bool isProperPair;
	int mappingQuality;
	bool passedFilters;
	bool isSecondMate;
	int chrNumber;
	int32_t position;
	int32_t lastPositionPlusOne;
	int32_t lastAlignedPositionWithRespectToRef;
	int32_t lastAlignedPos;
	std::string alignmentName;*/

	//TODO: move these functions to TGenome
	void fillReadGroupInfo(int & readGroupID);
	void binQualityScores(TQualityMap & qualityMap);
	void updateOptionalSamField(std::string tag, float value);
	void updateOptionalSamField(std::string tag, std::string value);
	void downsampleAlignment(double& fraction, TRandomGenerator& randomGenerator, TQualityMap & qualMap);

	void addToPMDTables(TPMDTables & pmdTables, TGenotypeMap & genoMap);
	void recalibrateWithPMD(TRecalibration* recalObject, TQualityMap & qualMap);
	double calculatePMDS(double & pi, TPMD* pmdObjects);
	void assessSoftClipping(int & S_left, int & middle, int & S_right, std::string & S_string_left, std::string & S_string_middle, std::string & S_qualities_middle, std::string & S_string_right, TGenotypeMap & genoMap);
	void removeSoftClippedBases(int & S_left, int & middle, int & S_right, std::string & S_string_left, std::string & S_string_middle, std::string & S_qualities_middle, std::string & S_string_right, TGenotypeMap & genoMap);
	int measureOverlap();
	void addToQualityTable(TQualityTable & qualTable, TQualityMap & qualMap);

	friend class TAlignmentParserSeqLib;
	friend class TWindowSeqLib;

};

#endif /* TALIGNMENT_H_SEQLIB */
