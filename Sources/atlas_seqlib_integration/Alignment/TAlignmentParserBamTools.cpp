/*
 * TAlignmentParser.cpp
 *
 *  Created on: Nov 14, 2017
 *      Author: wegmannd
 */

#include "TAlignmentParserBamTools.h"

//-----------------------------------------------------
//TAlignmentParserBamTools
//-----------------------------------------------------
TAlignmentParserBamTools::TAlignmentParserBamTools(){
	logfile = NULL;
	_keepDuplicates = false;
	_keepImproperPairs = false;
	_parse = false;
	previousAlignmentPos = -1;
	previousAlignmentChr = -1;
	oldAlignment = NULL;
	oldAlignmentInitialized = false;
	oldAlignmentMustBeConsidered = false;

	totalNumberAlignmentsRead = 0;
	sizeOfBamFile = 0;

	curReadGroupID = -1;

	//initialize iterators
	chrNumber = -1;
	chrLength = -1;

	//window parameters
	windowNumber = -1;
	windowSize = 0;
	numWindowsOnChr = -1;
	maxReadLength = 0;
	maxMissing = 1.0;
	maxRefN = 1.0;
	windowsPredefined = false;
	predefinedWindows = NULL;
	sitesProvided = false;

	//masks
	doMasking = false;
	doCpGMasking = false;
	considerRegions = false;
	mask = NULL;

	//filters
	applyDepthFilter = false;
	readUpToDepth = 10000;
	minDepth = 0;
	maxDepth = 10000;
	applyQualityFilter = false;
	minQual = 33;
	maxQual = 126;
	minPhredInt = 0;
	maxPhredInt = 93;
	minQualForPrinting = 33;
	maxQualForPrinting = 126;
	trimReads = false;
	trimmingLength3Prime = 0;
	trimmingLength5Prime = 0;
	applyFragmentLengthFilter = false;
	useStrand[0] = true; useStrand[1] = true;
	useMate[0] = true; useMate[1] = true;

	//blacklist
	_updateBlacklist = false;
	_writeBlackList = false;

	//limit chr and windows
	limitWindows = -1;
	skipWindows = 0;
	limitChr = -1;
	useChromosome = NULL;

	//reference
	hasReference = false;
	fastaReference = NULL;
	fastaBuffer = NULL;
	chrChanged = false;

	//post mortem damage and recalibration
	hasPMD = false;
	pmdObjects = NULL;
	doRecalibration = false;
	recalObjectInitialized = false;
	recalObject = NULL;
};

TAlignmentParserBamTools::TAlignmentParserBamTools(int MaxReadLength, TParameters & params, TLog* Logfile){
	TAlignmentParserBamTools();

	init(MaxReadLength, params, Logfile);
};

TAlignmentParserBamTools::~TAlignmentParserBamTools(){
	if(hasReference){
		delete fastaBuffer;
	}
	if(doMasking)
		delete mask;
	if(windowsPredefined)
		delete predefinedWindows;
	if(subset)
		delete subset;
	if(useChromosome)
		delete[] useChromosome;
	if(recalObjectInitialized)
		delete recalObject;
	if(pmdObjects)
		delete[] pmdObjects;
	if(oldAlignmentInitialized){
		delete oldAlignment;
	}

	if(_writeBlackList)
		ignoredReads.close();
}

void TAlignmentParserBamTools::init(int MaxReadLength, TParameters & params, TLog* Logfile){

	logfile = Logfile;

	//---------------------
	//Bamtools
	//---------------------

	//open BAM file
	filename = params.getParameterString("bam");
	openBamFile(filename);

//	//open FASTA reference
//	if(params.parameterExists("fasta")){
//		std::string fastaFile = params.getParameterString("fasta");
//		std::string fastaIndex = fastaFile + ".fai";
//		logfile->list("Reading reference sequence aaa from '" + fastaFile + "'");
//		if(!fastaReference.Open(fastaFile, fastaIndex)) throw "Failed to open FASTA file '" + fastaFile + "'! Is index file present?";
//		hasReference = true;
//		addReference(&fastaReference);
//	} else hasReference = false;

	//---------------------
	//window parameters
	//---------------------
	maxReadLength = MaxReadLength;
	oldAlignment = new TAlignmentBamTools(maxReadLength);
	oldAlignmentInitialized = true;

	if(!params.parameterExists("window") && params.parameterExists("windows")) logfile->warning("Argument 'windows' specified, but unknown. Did you mean 'window'?");
	std::string tmp = params.getParameterStringWithDefault("window", "1000000");
	//check if it is a number
	if(stringContainsOnly(tmp, "1234567890.Ee-+")){
		windowsPredefined = false;
		windowSize = stringToInt(tmp);
		logfile->list("Setting window size to " + toString(windowSize));
		if(windowSize < maxReadLength)
			throw "Window size " + tmp + " out of range! Windows must be at least as large as the max read length (" + toString(maxReadLength) + " bp)!";
	} else {
		windowsPredefined = true;
		logfile->listFlush("Limiting analysis to windows defined in '" + tmp + "'...");
		predefinedWindows = new TBed(tmp);
		logfile->done();
		logfile->conclude("read " + toString(predefinedWindows->size()) + " on " + toString(predefinedWindows->getNumChromosomes()) + " chromosomes");
	}
	numWindowsOnChr = 0;

	//--------------------
	//limit chrs and / or windows
	//--------------------

	useChromosome = new bool[bamHeader.Sequences.Size()];
	if(params.parameterExists("chr")){
		logfile->startIndent("Will limit analysis to the following chromosomes:");

		//set all chromosomes to false
		for(int i=0; i<bamHeader.Sequences.Size(); ++i)
				useChromosome[i] = false;

		//_parse chromosome names
		std::vector<std::string> vec;
		fillVectorFromString(params.getParameterString("chr"), vec, ',');
		for(std::vector<std::string>::iterator it=vec.begin(); it!=vec.end(); ++it){
			//find chromosome
			int num = 0;
			BamTools::SamSequenceIterator chrIt = bamHeader.Sequences.Begin();
			for( ; chrIt != bamHeader.Sequences.End(); ++chrIt, ++num){
				if(chrIt->Name == *it){
					useChromosome[num] = true;
					logfile->list(*it);
					break;
				}
			}
			if(chrIt == bamHeader.Sequences.End())
				throw "Chromosome '" + *it + "' is not present in the bam header!";
		}
		logfile->endIndent();
	} else {
		if(params.parameterExists("limitChr")){
			//set all chromosomes to false
			int num = 0;
			for(BamTools::SamSequenceIterator chrIt = bamHeader.Sequences.Begin(); chrIt != bamHeader.Sequences.End(); ++chrIt, ++num)
				useChromosome[num] = false;

			limitChr = params.getParameterIntWithDefault("limitChr", 1000000);
			if(limitChr > bamHeader.Sequences.Size())
				throw "Chromosome limit exceeds number of chromosomes in genome!";

			logfile->list("Will limit analysis to the first " + toString(limitChr) + " chromosomes.");

			num = 0;
			BamTools::SamSequenceIterator chrIt = bamHeader.Sequences.Begin();
			for( ; chrIt != bamHeader.Sequences.End(); ++chrIt, ++num){
				if(num == limitChr)
					break;
				useChromosome[num] = true;
				logfile->list(chrIt->Name);
			}

		} else {
			for(int i=0; i<bamHeader.Sequences.Size(); ++i)
				useChromosome[i] = true;
		}
	}

//	int num = 0;
//	for(BamTools::SamSequenceIterator chrIt = bamHeader.Sequences.Begin(); chrIt != bamHeader.Sequences.End(); ++chrIt, ++num)
//		std::cout << "useChromosome, num " << num << ": " << useChromosome[num] << std::endl;;


	skipWindows = params.getParameterIntWithDefault("skipWindows", 0);
	if(skipWindows > 0) logfile->list("Will skip the first " + toString(skipWindows) + " windows per chromosome.");
	limitWindows = params.getParameterLongWithDefault("limitWindows", 1000000000);
	if(params.parameterExists("limitWindows")) logfile->list("Will limit analysis to the first " + toString(limitWindows) + " windows per chromosome.");
	if(limitWindows <= skipWindows)
		throw "limitWwindows has to be larger than skipWindows!";


	//------------
	//masks
	//------------
	//normal mask
	if(params.parameterExists("mask")){
		if(windowsPredefined) throw "Masking is currently not implemented if windows are predefined from a BED file.";
		if(params.parameterExists("sites")) throw "Masking is currently not implemented if variant positions are also specified with \"sites\"";
		if(params.parameterExists("regions")) throw "Cannot use mask and regions at the same time";
		doMasking = true;
		std::string maskFile = params.getParameterString("mask");
		logfile->startIndent("Will mask all sites listed in BED file '" + maskFile + "':");
		logfile->listFlush("Reading file ...");
		mask = new TBedReader(maskFile, windowSize, bamHeader.Sequences, logfile);
		logfile->done();
		logfile->endIndent();
		//mask->print();
	} else doMasking = false;

	//reverse masking
	if(params.parameterExists("regions")){
		if(windowsPredefined) throw "Regions is currently not implemented if windows are predefined from a BED file.";
		if(params.parameterExists("sites")) throw "Regions is currently not implemented if variant positions are also specified with \"sites\"";
		considerRegions = true;
		std::string regionsFile = params.getParameterString("regions");
		logfile->startIndent("Will limit analysis to all regions listed in BED file '" + regionsFile + "':");
		logfile->listFlush("Reading file ...");
		mask = new TBedReader(regionsFile, windowSize, bamHeader.Sequences, logfile);
		logfile->done();
		logfile->endIndent();
	} else considerRegions = false;

	//CpG mask
	if(params.parameterExists("maskCpG")){
		if(!hasReference) throw "Cannot mask CpG sites without reference!";
		doCpGMasking = true;
		std::string maskFile = params.getParameterString("maskCpG");
		logfile->list("Will mask all CpG sites");
	} else doCpGMasking = false;

	//-------------
	//sites
	//-------------

	//only call at specific sites?
	if(params.parameterExists("invariantSites") && params.parameterExists("variantSites"))
		throw "Can only use variant OR invariant sites!";
	if(params.parameterExists("invariantSites")){
		bool variantSites = false;
		if(hasReference)
			subset = new TSiteSubset(params.getParameterString("invariantSites"), *fastaReference, bamHeader, windowSize, logfile, variantSites);
		else subset = new TSiteSubset(params.getParameterString("invariantSites"), windowSize, logfile, variantSites);
		sitesProvided = true;
	} else if(params.parameterExists("variantSites")){
		bool variantSites = true;
		if(hasReference)
			subset = new TSiteSubset(params.getParameterString("variantSites"), *fastaReference, bamHeader, windowSize, logfile, variantSites);
		else subset = new TSiteSubset(params.getParameterString("variantSites"), windowSize, logfile, variantSites);
		sitesProvided = true;
	}

	//------------
	//filters
	//------------
	//depth filter
	readUpToDepth = params.getParameterIntWithDefault("readUpToDepth", 1000);
	if(params.parameterExists("minDepth") || params.parameterExists("maxDepth")){
		applyDepthFilter = true;
		unsigned int tmpInt;
		tmpInt = params.getParameterIntWithDefault("minDepth", 0);
		if(tmpInt < 0) throw "minDepth must be >= 0!";
		minDepth = tmpInt;
		tmpInt = params.getParameterIntWithDefault("maxDepth", 1000000);
		if(tmpInt < minDepth) throw "maxDepth must be >= minDepth!";
		maxDepth = tmpInt;
		readUpToDepth = maxDepth + 1;
		logfile->list("Will filter out sites with sequencing depth < " + toString(minDepth) + " or > " + toString(maxDepth));
	} else {
		applyDepthFilter = false;
		minDepth = 0;
		maxDepth = 1000000;
	}
	logfile->list("Will read data up to depth " + toString(readUpToDepth) + " and ignore additional bases.");

	//quality filters
	minPhredInt = params.getParameterIntWithDefault("minQual", 1);
	if(minPhredInt < 0) throw "minQual must be >= 0!";
	maxPhredInt = params.getParameterIntWithDefault("maxQual", 93);
	if(maxPhredInt < minPhredInt) throw "maxQual must be >= minQual!";
	setQualityFilters(minPhredInt, maxPhredInt);
	logfile->list("Will filter out bases with quality outside the range [" + toString(minPhredInt) + ", " + toString(maxPhredInt) + "]");

	//quality filters for printing
	int minOutQual = params.getParameterIntWithDefault("minOutQual", 1) + 33;
	if(minOutQual < 0) throw "minOutQual must be >= 0!";
	int maxOutQual = params.getParameterIntWithDefault("maxOutQual", 93) + 33;
	if(maxOutQual < minOutQual) throw "maxOutQual must be >= minOutQual!";
	setQualityRangeForPrinting(minOutQual, maxOutQual);
	logfile->list("Will print qualities truncated to [" + toString(minOutQual) + ", " + toString(maxOutQual) + "]");

	//filter for missing reference
	maxMissing = params.getParameterDoubleWithDefault("maxMissing", 1.0);
	if(maxMissing > 1.0) throw "maxMissing must be smaller or equal to 1.0!";

	maxRefN = params.getParameterDoubleWithDefault("maxRefN", 1.0);
	if(maxRefN > 1.0) throw "maxRefN must be smaller or equal to 1.0!";
	if(maxRefN < 1.0 && hasReference == false) throw "Can only calculate percentage of reference bases that are 'N' in window if reference file is provided.";

	//-----------------
	//read groups
	//-----------------

	readGroups.fill(bamHeader);

	//limit readGroups
	if(params.parameterExists("readGroup")){
		readGroups.filterReadGroups(params.getParameterString("readGroup"));
		logfile->startIndent("Will limit analysis to the following read groups:");
		readGroups.printReadgroupsInUse(logfile);
		logfile->endIndent();
	}

	//------------
	//recal and pmd
	//------------

	initializePostMortemDamage(params);
	initializeRecalibration(params);

	//------------
	//other
	//------------

	if(params.parameterExists("keepDuplicates")){
		keepDuplicates();
		logfile->list("Will keep duplicate reads.");
	}

	if(params.parameterExists("dontFilterReadsLongerFragment"))
		setApplyFragmentLengthFilter(false);
	else
		setApplyFragmentLengthFilter(true);

	//strand
	if(params.parameterExists("keepOnlyFwd")){
		useStrand[1] = false;
		logfile->list("Will keep only forward mapping reads.");
	}
	else if(params.parameterExists("keepOnlyRev")){
		useStrand[0] = false;
		logfile->list("Will keep only reverse mapping reads.");
	}

	//mate
	if(params.parameterExists("keepOnlyFirst")){
		useMate[1] = false;
		logfile->list("Will keep only the first mates.");
	}
	else if(params.parameterExists("keepOnlySecond")){
		useMate[0] = false;
		logfile->list("Will keep only the second mates.");
	}
};

void TAlignmentParserBamTools::openBamFile(std::string filename){
	//open BAM file
	logfile->list("Reading data from BAM file '" + filename + "'.");
	if (!bamReader.Open(filename))
		throw "Failed to open BAM file '" + filename + "'!";
	//load index file
	if(!bamReader.LocateIndex())
		throw "No index file found for BAM file '" + filename + "'!";

	//initialize bam stuff
	bamHeader = bamReader.GetHeader();

	//get file size
	chrIterator = bamHeader.Sequences.End() - 1;
	bamReader.Jump(bamHeader.Sequences.Size() - 1, 0);
	BamTools::BamAlignment bamAlignment;
	bamReader.GetNextAlignment(bamAlignment);
	sizeOfBamFile = bamReader.tell();
	bamReader.Rewind();

	//set iterator to end
	chrIterator = bamHeader.Sequences.End();
}

void TAlignmentParserBamTools::setQualityFilters(int MinPhredInt, int MaxPhredInt){
	applyQualityFilter = true;
	minPhredInt = MinPhredInt;
	maxPhredInt = MaxPhredInt;
	minQual = qualMap.phredIntToQuality(minPhredInt);
	maxQual = qualMap.phredIntToQuality(maxPhredInt);
};

void TAlignmentParserBamTools::setQualityRangeForPrinting(int minQual, int maxQual){
	minQualForPrinting = minQual;
	maxQualForPrinting = maxQual;
};

void TAlignmentParserBamTools::setReadTrimming(int trim3Prime, int trim5Prime){
	trimmingLength3Prime = trim3Prime;
	trimmingLength5Prime = trim5Prime;
	trimReads = true;
};

void TAlignmentParserBamTools::setApplyFragmentLengthFilter(bool filterYesNo){
	applyFragmentLengthFilter = filterYesNo;
}

void TAlignmentParserBamTools::addReference(BamTools::Fasta* reference){
	hasReference = true;
	fastaReference = reference;
	fastaBuffer = new TFastaBuffer(reference);
};

void TAlignmentParserBamTools::fillReferenceSequence(TFastaBuffer* fastaBuffer, TAlignment & alignment){
	if(!hasReference) //is this check really necessary?
		throw "No reference provided!";
	std::string referenceSequence;
	fastaBuffer->fill(alignment.chrNumber, alignment.position, alignment.position + alignment.bases[alignment.length-1].alignedPos, referenceSequence);
	alignment.referenceSequence = referenceSequence;
	alignment.hasReference = true;
};

std::string TAlignmentParserBamTools::chrNumberToName(int chrNumber){
	int counter = 0;
	for(BamTools::SamSequenceIterator chrIt=bamHeader.Sequences.Begin(); chrIt!=bamHeader.Sequences.End(); ++chrIt, ++counter){
		if(counter == chrNumber)
			return chrIt->Name;
	}
	throw "chrNumber not in header";
};

int TAlignmentParserBamTools::chrNumberToLength(int chrNumber){
	int counter = 0;
	for(BamTools::SamSequenceIterator chrIt=bamHeader.Sequences.Begin(); chrIt!=bamHeader.Sequences.End(); ++chrIt, ++counter){
		if(counter == chrNumber)
			return stringToInt(chrIt->Length);
	}
	throw "chrNumber not in header";
};

long TAlignmentParserBamTools::calcReferenceLength(){
	int chrNum = 0;
	long totLength = 0;
    for(chrIterator = bamHeader.Sequences.Begin(); chrIterator!=bamHeader.Sequences.End(); ++chrIterator, ++chrNum)
        if(useChromosome[chrNum]) totLength += stringToLong(chrIterator->Length);
    return totLength;
};

//--------------
//move genome
//--------------
void TAlignmentParserBamTools::jumpToEnd(){
	chrIterator = bamHeader.Sequences.End();
	chrNumber = -1;
};

void TAlignmentParserBamTools::restartChromosomes(TWindow & window){
	chrIterator = bamHeader.Sequences.Begin();
	chrNumber = 0;

	moveChromosome(window);
};

void TAlignmentParserBamTools::moveChromosome(TWindow & window){
	//jump reader
	oldAlignmentMustBeConsidered = false;
	chrLength = stringToLong(chrIterator->Length);

	//restart windows
	previousAlignmentPos = -1;
	windowNumber = 0;

	if(windowsPredefined){
		//find next used chromosome with windows
		do {
			predefinedWindows->setChr(chrIterator->Name);
			numWindowsOnChr = predefinedWindows->getNumWindowsOnCurChr();
			if(numWindowsOnChr < 1 || useChromosome[chrNumber] == false ){
				if(useChromosome[chrNumber])
					logfile->conclude("No windows on chromosome " + chrIterator->Name + ".");
				++chrIterator;
				++chrNumber;
				if(chrIterator == bamHeader.Sequences.End())
					return;

				predefinedWindows->setChr(chrIterator->Name);
				numWindowsOnChr = predefinedWindows->getNumWindowsOnCurChr();
			}
		} while((numWindowsOnChr < 1 || !useChromosome[chrNumber]) && chrIterator != bamHeader.Sequences.End());

		//now jump
		window.move(predefinedWindows->curWindowStart(), predefinedWindows->curWindowEnd(), chrNumber);
		bamReader.Jump(chrNumber, window.start);

	} else {
		while(!useChromosome[chrNumber] || skipWindows * windowSize > chrLength){
			++chrIterator;
			++chrNumber;
			chrLength = stringToLong(chrIterator->Length);
			chrLength = stringToLong(chrIterator->Length);
		}
		window.chrName = chrIterator->Name;
		numWindowsOnChr = ceil(chrLength / (double) windowSize);

		int curStart = skipWindows * windowSize;
		bamReader.Jump(chrNumber, curStart);
		int nextEnd = curStart + windowSize;
		//TODO:!!! removed +1 because we are zero-based. Check if true!
		if(nextEnd > chrLength){
			nextEnd = chrLength;
		}
		window.move(curStart, nextEnd, chrNumber);
	}

	if(chrIterator == bamHeader.Sequences.End())
		return;

	//advance mask
	if(doMasking || considerRegions) mask->setChr(chrIterator->Name);
	if(sitesProvided) subset->setChr(chrIterator->Name);

	//write progress
	logfile->endIndent();
	logfile->startNumbering("Parsing chromosome '" + chrIterator->Name + "':");
};

bool TAlignmentParserBamTools::moveToNextWindowOnChr(TWindow & window){

	if(window.end > 0) logfile->endIndent();


	//if sites defined
	int counter = 0;
	do{
		//move possible?
		++windowNumber;
		++counter;
	} while(sitesProvided && !subset->hasPositionsInWindow(window.end) && window.end + window.length * counter < chrLength);

	if(window.end >= chrLength || windowNumber >= limitWindows)
		return false;

	//calculate new end
	long nextEnd = window.end + windowSize;
	if(nextEnd > chrLength)
		nextEnd = chrLength;
	window.move(window.end, nextEnd, chrNumber);

	return true;
};

bool TAlignmentParserBamTools::moveToNextPredefinedWindow(TWindow & window){

	if(window.end > 0) logfile->endIndent();

	++windowNumber;
	if(windowNumber >= limitWindows)
		return false;
	if(predefinedWindows->nextWindow()){
		window.move(predefinedWindows->curWindowStart(), predefinedWindows->curWindowEnd(), chrNumber);
		//should we jump or are we already close enough to next window
		if(abs(window.start - previousAlignmentPos) > maxReadLength){
			previousAlignmentPos = -1;
			if(window.start - maxReadLength < 0)
				bamReader.Jump(chrNumber, 0);
			else{
				bamReader.Jump(chrNumber, window.start - maxReadLength);
			}
		}
		return true;
	} else
		return false;
};

bool TAlignmentParserBamTools::moveWindow(TWindow & window){
	//returns false when end of genome is reached
	if(windowsPredefined){
		//if at beginning of BAM file
		if(chrIterator == bamHeader.Sequences.End()){
			restartChromosomes(window);

			if(chrIterator == bamHeader.Sequences.End())
				throw "found no predefined windows in BED file! Does file exist?";

		} else {
			//now move coordinates of next window
			if(!moveToNextPredefinedWindow(window)){
				//no more windows left on chr
				++chrIterator;
				++chrNumber;

				if(chrIterator == bamHeader.Sequences.End())
					return false;

				moveChromosome(window);


				if(chrIterator == bamHeader.Sequences.End())
					return false;
				++windowNumber;
			}
		}

	} else {
		//if at beginning of BAM file
		if(chrIterator == bamHeader.Sequences.End())
			restartChromosomes(window);
		else {
			if(!moveToNextWindowOnChr(window)){
				//there is no window left on chr
				++chrIterator;
				++chrNumber;

				//do we use this chromosome? if not, move on!
				while(chrIterator != bamHeader.Sequences.End() && !useChromosome[chrNumber]){
					++chrIterator;
					++chrNumber;
				}

				//did we reach end?
				if(chrIterator == bamHeader.Sequences.End() || (limitChr != -1 && chrNumber >= limitChr)){
					window.end = 0;
					return false;
				}
				moveChromosome(window);
			}
		}
	}

	//report
	logfile->number("Window [" + toString(window.start) + ", " + toString(window.end) + ") of " + toString(numWindowsOnChr) + " on '" + chrIterator->Name + "':");
	logfile->addIndent();
	return true;
};

//------------------------------
//reading alignments
//------------------------------
bool TAlignmentParserBamTools::readAlignment(){
	bool filtersPassed = false;
	do {
		if(!bamReader.GetNextAlignment(bamAlignment)){
			return false;
		}
		++totalNumberAlignmentsRead;

		//check if bam file is sorted
		if(bamAlignment.RefID != previousAlignmentChr){
			previousAlignmentPos = -1;
			previousAlignmentChr = bamAlignment.RefID;
//			chrNumber = previousAlignmentChr;
			chrChanged = true;
		} else
			chrChanged = false;

		if(bamAlignment.Position < previousAlignmentPos)
			throw "BAM file must be sorted by position! Alignment '" + bamAlignment.Name + "' is at position " + toString(bamAlignment.Position) + ", which is before the position of the previous alignment (" + toString(previousAlignmentPos) + ")";
		previousAlignmentPos = bamAlignment.Position;

//		//jump to next chromosome if not in use
//		if(previousAlignmentChr != -1 && !useChromosome[chrNumber]){
//			std::cout << "entered correct if statement" << std::endl;
//			while(chrNumber < bamHeader.Sequences.Size() && !useChromosome[chrNumber]){
//				++chrIterator;
//				++chrNumber;
//
//				std::cout << "chrNumber is now " << chrNumber << std::endl;
//				std::cout << "getReferenceCount " << bamReader.GetReferenceCount() << std::endl;
//
//			}
//			if(!bamReader.Jump(chrNumber, 0))
//				throw "jump didnt work!";
//			std::cout << "jumping to chrNumber " << chrNumber << std::endl;
//			std::cout << "getReferenceCount " << bamReader.GetReferenceCount() << std::endl;
//		}

		//check read length
		/* check if insert size is shorter than read-insertions+deletions=alignedBases.length() -> this means we are reading the adaptor sequence.
		Insert size is determined by mapping -> insertions are not in ref and should not count. If we don't add deletions, adapter at end could be sequenced but we still keep read
		(deletions in aligned bases are represented as dashes) */
		if(bamAlignment.AlignedBases.size() > maxReadLength)
			throw "Alignment '" +  bamAlignment.Name + "' is longer than the max read length! Please change max read length to parse this data.";

		//store read group ID
		std::string readGroup;
		bamAlignment.GetTag("RG", readGroup);
		curReadGroupID = readGroups.find(readGroup);

		//filter
		//TODO: add functionality to not filter at all (i.e. _keepAll switch)
		filtersPassed = true;
		/* check if insert size is shorter than read-insertions+deletions=alignedBases.length() -> this means we are reading the adaptor sequence.
		Insert size is determined by mapping -> insertions are not in ref and should not count. If we don't add deletions, adapter at end could be sequenced but we still keep read
		(deletions in aligned bases are represented as dashes) */
		if(bamAlignment.IsPaired() && applyFragmentLengthFilter && abs(bamAlignment.InsertSize) < bamAlignment.AlignedBases.length()){
			logfile->warning("The following alignment is longer than its insert size: " + bamAlignment.Name);
			filtersPassed = false;
			if(_updateBlacklist)
				addToBlacklist(bamAlignment, "longer than insert size (TLEN)");
		} else {
			//apply filters: read group in use and basic QC
			filtersPassed = applyFilters();
			if(_updateBlacklist && !filtersPassed){
				addToBlacklist(bamAlignment, "did not pass parser filters");
			}
		}
	} while(!filtersPassed);

	return true;
};

bool TAlignmentParserBamTools::applyFilters(){
	bool filtersPassed = readGroups.readGroupInUse(curReadGroupID)
					&& (_keepImproperPairs || (!bamAlignment.IsPaired() || bamAlignment.IsProperPair()))
					&& bamAlignment.IsMapped()
					&& !bamAlignment.IsFailedQC()
					&& bamAlignment.IsPrimaryAlignment()
					&& !bamAlignment.IsSupplementary()
					&& useChromosome[bamAlignment.RefID]
					&& (_keepDuplicates || !bamAlignment.IsDuplicate())
					&& useStrand[bamAlignment.IsReverseStrand()]
					&& useMate[bamAlignment.IsSecondMate()];

	return filtersPassed;
};

void TAlignmentParserBamTools::fillAlignment(TAlignment & _alignment){
	
	//cast
	TAlignmentBamTools alignment = (TAlignmentBamTools) _alignment;

	//make sure container is empty
	alignment.clear();

	//fill alignment
	std::string readGroup;
	bamAlignment.GetTag("RG", readGroup);
	int readGroupId = readGroups.find(readGroup);

	alignment.fill(bamAlignment, readGroupId);

	if(_parse){
		//add all info from bamAlignment to bases
		alignment.parse(genoMap, qualMap);

		//add missing information to bases
		alignment.fillReadGroupInfo(readGroupId);
		alignment.fillPmdProbabilities(pmdObjects);

		if(applyQualityFilter)
			alignment.filterForBaseQuality(minQual, maxQual);
		if(doRecalibration)
			recalibrate(alignment);
		if(hasReference)
			fillReferenceSequence(fastaBuffer, alignment);
	}
};

//------------------------
//read data in alignments
//------------------------

bool TAlignmentParserBamTools::readNextAlignment(TAlignment & alignment){
	//use this in TGenome for functionalities that don't need windows
	if(readAlignment()){
		alignment.passedFilters = true;
		fillAlignment(alignment);
		return true;
	}
	return false;
};

bool TAlignmentParserBamTools::readNextAlignmentWithBlacklist(TAlignment & alignment){
	//use this in TGenome for functionalities that don't need windows
	if(readAlignment()){
		alignment.passedFilters = true;
		fillAlignment(alignment);
		return true;
	} else if(!readAlignment()){
		blacklist.emplace(bamAlignment.Name, 1);
	}
	return false;
};

//---------------------
//read data in windows
//---------------------
bool TAlignmentParserBamTools::readDataInNextWindow(TWindow & window){
	setParsingToTrue();

	//move window
	if(!moveWindow(window)){
		return false;
	}

	//read data
	readAlignmentsIntoWindow(window);

	return true;
};


void TAlignmentParserBamTools::readAlignmentsIntoWindow(TWindow & _window){

	TWindowBamTools window;
	
	try{
        	window = dynamic_cast<TWindowBamTools&>(_window);
    	} catch(const std::bad_cast& ex){
       		std::cout << "["<<ex.what()<<"]" << std::endl;
    	}

	//measure runtime
	struct timeval start, end;
	gettimeofday(&start, NULL);
	logfile->listFlush("Reading data ...");

	//check if old alignment is to be used.
	if(oldAlignmentMustBeConsidered){
		if(bamAlignment.Position >= window.end){
			return;
		}

		oldAlignmentMustBeConsidered = false;
		if(oldAlignment->lastAlignedPositionWithRespectToRef >= window.start)
			oldAlignment = window.swapUsedForEmptyAlignment(oldAlignment, maxReadLength);
	}

	//read alignments
	int counter = 0;
	while(readAlignment()){
		//fill alignment
		fillAlignment(*oldAlignment);

		++counter;

		//check if alignment starts after current window end -> break
		if(oldAlignment->position >= window.end || oldAlignment->chrNumber != window.chrNumber){
			oldAlignmentMustBeConsidered = true;
			break;
		}

		//check if alignment contains part of the window
		//if read continues outside of window, this is dealt with by window object
		if(oldAlignment->position >= window.start || oldAlignment->lastAlignedPositionWithRespectToRef >= window.start){
			oldAlignment = window.swapUsedForEmptyAlignment(oldAlignment, maxReadLength);
		}
	}

	//fill sites
	if(sitesProvided){
		window.fillSitesSubset(subset, readUpToDepth);
		window.addReferenceBaseToSites(subset);
	} else {
		window.fillSites(readUpToDepth);
		if(hasReference)window.addReferenceBaseToSites(*fastaReference);
	}

	//report
	gettimeofday(&end, NULL);
	logfile->write(" done (in " , end.tv_sec  - start.tv_sec, "s)!");

	//apply filters
	applyFilters(window);
};


void TAlignmentParserBamTools::applyFilters(TWindow & window){
	window.passedFilters = false;
	if(window.numReadsInWindow > 0){
		//apply masks and filters
		if(doMasking){
			logfile->listFlush("Masking sites ...");
			window.applyMask(mask, considerRegions);
			logfile->done();
		} else if(considerRegions){
			logfile->listFlush("Masking sites outside regions ...");
			window.applyMask(mask, considerRegions);
			logfile->done();
		} else if(doCpGMasking){
			logfile->listFlush("Masking CpG sites ...");
			window.maskCpG();
			logfile->done();
		} if(applyDepthFilter){
			window.applyDepthFilter(minDepth, maxDepth);
		} if(maxRefN < 1.0 && hasReference == true){
			window.calcFracN();
		}

		//calc sequencing depth
		window.calcDepth();

		//report
		logfile->conclude("read data from " + toString(window.numReadsInWindow) + " reads.");
		logfile->conclude("sequencing depth is " + toString(window.depth));
		logfile->conclude(toString(window.fractionsitesDepthAtLeastTwo * 100) + "% of all sites are covered at least twice");
		logfile->conclude(toString(window.fractionSitesNoData * 100) + "% of all sites have no data");
		if(window.fractionSitesNoData > maxMissing){
			logfile->conclude("Level of missing data > threshold of " + toString(maxMissing) + " -> skipping this window");
			return;
		}
		if(maxRefN < 1.0 && hasReference == true){
			logfile->conclude(toString(window.fractionRefIsN * 100) + "% of all reference bases are 'N'");
			if(window.fractionRefIsN > maxRefN){
				logfile->conclude("Fraction of 'N' in reference > threshold of " + toString(maxRefN) + " -> skipping this window");
				return;
			}
		}
		window.passedFilters = true;
	} else {
		logfile->conclude("No data in this window.");
	}
}

//------------------------------
//initialize PMD and recalibration
//------------------------------

PMDType TAlignmentParserBamTools::getEnumPMDType(std::string pmdType){
	if(pmdType == "CT")
		return pmdCT;
	else if(pmdType == "GA")
		return pmdGA;
	else if(pmdType == "GT")
		return pmdGT;
	else if(pmdType == "CA")
		return pmdCA;
	else {
		throw "unknown pmdType: " + pmdType + "!";
	}
}

void TAlignmentParserBamTools::initializePostMortemDamage(TParameters & params){
	logfile->startIndent("Initializing Post Mortem Damage (PMD):");
	//create an array of TPMD objects for each read group
	pmdObjects = new TPMD[readGroups.size()];

	//now fill them!
	if(params.parameterExists("pmd") || params.parameterExists("pmdCT") || params.parameterExists("pmdGA")){
		//all read groups have the same pmd
		logfile->list("Initializing one PMD function for all read groups.");
		pmdObjects[0].initialize(params, logfile);
		for(int i=1; i<readGroups.size(); ++i)
			pmdObjects[i].initialize(pmdObjects[0]);
		hasPMD = true;
	} else if(params.parameterExists("pmdFile")){
		//read from file for each read group
		std::string filename = params.getParameterString("pmdFile");
		logfile->list("Reading PMD from file '" + filename + "'.");
		std::ifstream file(filename.c_str());
		if(!file) throw "Failed to open PMD file '" + filename + "'!";

		//parse file that has structure: readGroup PMD(CT) PMD(GA)
		int lineNum = 0;
		std::string line;
		std::vector<std::string> vec;
		int readGroupId;
		while(file.good() && !file.eof()){
			++lineNum;
			//skip empty lines or those that start with //
			std::getline(file, line);
			line = extractBefore(line, "//");
			trimString(line);
			if(!line.empty()){
				fillVectorFromStringWhiteSpaceSkipEmpty(line, vec);
				if(vec.size() != 3) throw "Found " + toString(vec.size()) + " instead of 3 columns in '" + filename + "' on line " + toString(lineNum) + "!";
				if(readGroups.readGroupExists(vec[0])){ //ignore if it does not exist
					//get read group and PMD type
					readGroupId = readGroups.find(vec[0]);
					if(!params.parameterExists("oldPMDFormat")){
						PMDType pmdType = getEnumPMDType(vec[1]);
						//initialize functions
						pmdObjects[readGroupId].initializeFunction(vec[2], pmdType);
					} else {
						//initialize functions
						pmdObjects[readGroupId].initializeFunction(vec[1], pmdCT);
					//	logfile->conclude("For read group '" + vec[0] + "', C->T: " + pmdObjects[readGroupId].getFunctionString(pmdCT));
						pmdObjects[readGroupId].initializeFunction(vec[2], pmdGA);
					//	logfile->conclude("For read group '" + vec[0] + "', G->A: " + pmdObjects[readGroupId].getFunctionString(pmdGA));
					}
				}
			}
		}

		//close file
		file.close();

		//test if we have a function for all read groups
		for(int i=0; i<readGroups.size(); ++i){
			if(!pmdObjects[i].functionInitialized(pmdCT)) throw "PMD C->T for read group '" + readGroups.getName(i) + "' is missing in file '" + filename + "'!";
			if(!pmdObjects[i].functionInitialized(pmdGA)) throw "PMD G->A for read group '" + readGroups.getName(i) + "' is missing in file '" + filename + "'!";
		}
		hasPMD = true;
	} else {
		//no post mortem damage
		logfile->list("Assuming there is no PMD in the data.");
		std::string pmdString = "none";
		for(int i=0; i<readGroups.size(); ++i){
			pmdObjects[i].initializeFunction(pmdString, pmdGA);
			pmdObjects[i].initializeFunction(pmdString, pmdCT);
		}
	}
	logfile->endIndent();
}

void TAlignmentParserBamTools::initializeRecalibration(TParameters & params){
	std::string task = params.getParameterString("task");
	if(params.parameterExists("BQSRQuality")){
		recalObject = new TRecalibrationBQSR(params, logfile, &readGroups);
		doRecalibration = true;
	} else if(params.parameterExists("recal")){
		recalObject = new TRecalibrationEM(params.getParameterString("recal"), &readGroups, logfile);
		doRecalibration = true;
	} else {
		logfile->list("Assuming that error rates in BAM files are correct (no recalibration).");
		doRecalibration = false;
		recalObject = new TRecalibration();
	}
	recalObjectInitialized = true;

	//check if estimation is required, in which case throw an error!
	if(recalObject->_requiresEstimation()) throw "Can not use provided recalibration: estimation is required!";
};

void TAlignmentParserBamTools::recalibrate(TAlignment & alignment){
	//make sure read is parsed and has reference
	if(!alignment.parsed) throw "Read was not parsed!";

	if(recalObject->recalibrationChangesQualities()){
		//recalibrate quality scores
		for(int d=0; d<alignment.length; ++d){
			if(alignment.bases[d].aligned && alignment.bases[d].base != N){
				alignment.bases[d].errorRate = recalObject->getErrorRate(alignment.bases[d]);
			}
		}

		alignment.changed = true;
	} else alignment.changed = false;
	alignment.recalibrated = true;
};

void TAlignmentParserBamTools::addSitesToQualityTransformTable(TAlignment & alignment, TQualityTransformTables & QTtables){
	for(int i=0; i<alignment.length; ++i){
		if(alignment.bases[i].base != N){
			int newQual = qualMap.errorToQuality(alignment.bases[i].errorRate);
			QTtables.add(alignment.readGroupId, alignment.qualityOriginal[i], newQual);
		}
	}
};

void TAlignmentParserBamTools::addSitesToQualityTransformTable(TAlignment & alignment, TRecalibration* otherRecalObject, TQualityTransformTables & QTtables){
	for(int i=0; i<alignment.length; ++i){
		if(alignment.bases[i].base != N){
			int firstQual = qualMap.errorToQuality(alignment.bases[i].errorRate);
			double tmp = alignment.bases[i].errorRate;
			alignment.bases[i].errorRate = qualMap.qualityToError(alignment.qualityOriginal[i]);
			int secondQual = otherRecalObject->getQuality(alignment.bases[i]);
			alignment.bases[i].errorRate = tmp;
			QTtables.add(alignment.readGroupId, firstQual, secondQual);
		}
	}
};

void TAlignmentParserBamTools::adaptQualityWhenMerging(TBase & bestBase, TBase & worstBase, const bool & adaptQuality){
	if(adaptQuality){
		double likelihood[4];
		double sum = 0.0;
		for(int i=0; i<4; ++i){
			if(bestBase.base == i){
				likelihood[i] = 1.0 - bestBase.errorRate;
			} else {
				likelihood[i] = bestBase.errorRate / 3.0;
			}

			if(worstBase.base == i){
				likelihood[i] *= 1.0 - worstBase.errorRate;
			} else {
				likelihood[i] *= worstBase.errorRate / 3.0;
			}
			sum += likelihood[i];

		}
		bestBase.errorRate = 1.0 - likelihood[bestBase.base] / sum;
	}

	worstBase.errorRate = 1.0;
	worstBase.base = N;
}

void TAlignmentParserBamTools::mergeAlignedBasesBamReads(TAlignment* fwdAlignment, TAlignment* revAlignment, bool adaptQuality){
	//deletions and insertions are kept as is. these positions are not compared
	if(fwdAlignment->lastAlignedPositionWithRespectToRef >= revAlignment->position){
		fwdAlignment->setAlignmentHasChanged();
		revAlignment->setAlignmentHasChanged();

		//reads overlap -> check if there are bases overlapping same position in ref
		//alignedPos is with respect to read
		int fwdP = 0;
		int revP = 0;
		while(fwdP <= fwdAlignment->lastAlignedPos && revP <= revAlignment->lastAlignedPos){
			if(fwdAlignment->position + fwdAlignment->bases[fwdP].alignedPos == revAlignment->position + revAlignment->bases[revP].alignedPos){
				//bases overlap same position in ref -> decide which one to keep
				if(fwdAlignment->bases[fwdP].errorRate < revAlignment->bases[revP].errorRate){
					adaptQualityWhenMerging(fwdAlignment->bases[fwdP], revAlignment->bases[revP], adaptQuality);
				} else {
					adaptQualityWhenMerging(revAlignment->bases[revP], fwdAlignment->bases[fwdP], adaptQuality);
				}
				//increment both counters
				++fwdP;
				++revP;
			} else if(fwdAlignment->position + fwdAlignment->bases[fwdP].alignedPos < revAlignment->position + revAlignment->bases[revP].alignedPos){
				++fwdP;
			} else {
				++revP;
			}
		}
	}
}
