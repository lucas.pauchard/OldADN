/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "TAlignmentParser.h"


//-----------------------------------------------------
//TFastaBuffer
//-----------------------------------------------------
TFastaBuffer::TFastaBuffer(BamTools::Fasta* Reference){
	bufferSize = 100000;
	reference = Reference;
	referenceSequence = "";
	curStart = -1;
	curChr = -1;
	curEnd = -1;
};

void TFastaBuffer::moveTo(const int & chr, const int32_t & pos){
	curChr = chr;
	curStart = pos;
	curEnd = pos + bufferSize;
	if(!reference->GetSequence(chr, curStart, curEnd, referenceSequence))
		throw "Problem reading " + toString(chr) + ":" + toString(curStart) + "-" + toString(curEnd) + " from fasta file!";
};

void TFastaBuffer::fill(const int & chr, const int32_t & start, const int32_t end, std::string & ref){
	//move buffer, if necessary
	if(chr != curChr || end > curEnd || start < curStart){
		if(end - start + 1 > bufferSize){
			bufferSize = end - start + 1;
		}
		moveTo(chr, start);
	}

	//now copy to string
	ref.assign(referenceSequence, start - curStart, end - start + 1);
};


//-----------------------------------------------------
// TBamProgressReporter
//-----------------------------------------------------
TBamProgressReporter::TBamProgressReporter(int Frequency, TAlignmentParser* Parser, TLog* Logfile){
	_init(Frequency, Parser, Logfile);
};

TBamProgressReporter::TBamProgressReporter(TAlignmentParser* Parser, TLog* Logfile){
	_init(1000000, Parser, Logfile);
};

void TBamProgressReporter::_init(int Frequency, TAlignmentParser* Parser, TLog* Logfile){
	progressFrequency = Frequency;
	parser = Parser;
	logfile = Logfile;
	gettimeofday(&start, NULL);
	lastProgressPrinted = 0;

	logfile->startIndent("Parsing through BAM file:");
};

std::string TBamProgressReporter::_getRunTime(){
	gettimeofday(&end, NULL);
	return to_string_with_precision((end.tv_sec  - start.tv_sec)/60.0, 2);
};

void TBamProgressReporter::_printProgress(){
	std::string percentOfFile = to_string_with_precision(parser->getPositionInFile() * 100, 2);
	std::string millionReads = to_string_with_precision((double) parser->getNumAlignmentsRead() / 1000000.0, 1);
	logfile->list("Parsed " + millionReads + " million reads (est. " + percentOfFile + "%) in " + _getRunTime() + " min.");
};

void TBamProgressReporter::printProgress(){
	if(parser->getNumAlignmentsRead() - lastProgressPrinted >= progressFrequency){
		_printProgress();
		lastProgressPrinted = parser->getNumAlignmentsRead();
	}
};

void TBamProgressReporter::printEnd(){
	logfile->list("Reached end of BAM file.");
	logfile->conclude("Parsed a total of " + toString((double) parser->getNumAlignmentsRead()) + " reads in " + _getRunTime() + " min.");
	logfile->endIndent("Reached end of BAM file.");
};