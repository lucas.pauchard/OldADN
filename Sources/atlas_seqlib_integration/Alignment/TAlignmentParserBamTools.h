/*
 * TAlignmentParser.h
 *
 *  Created on: Nov 14, 2017
 *      Author: wegmannd
 */

#ifndef TALIGNMENTPARSER_H_BAMTOOLS
#define TALIGNMENTPARSER_H_BAMTOOLS

#include "TAlignmentParser.h"


//-----------------------------------------------------
//TAlignmentParserBamTools
//-----------------------------------------------------
class TAlignmentParserBamTools : public TAlignmentParser{
private:

	//contructor functions
	void openBamFile(std::string filename);

	//move genome
	void jumpToEnd();
	void restartChromosomes(TWindow & window);
	void moveChromosome(TWindow & window);
	bool moveToNextWindowOnChr(TWindow & window);
	bool moveToNextPredefinedWindow(TWindow & window);
	bool moveWindow(TWindow & window);

	PMDType getEnumPMDType(std::string pmdType);
	void initializePostMortemDamage(TParameters & params);
	void initializeRecalibration(TParameters & params);

	bool readAlignment();
	bool applyFilters();
	void fillAlignment(TAlignment & alignment);
	void readAlignmentsIntoWindow(TWindow & window);
	void applyFilters(TWindow & window);
	void adaptQualityWhenMerging(TBase & bestBase, TBase & worstBase, const bool & adaptQuality);

	//blacklist
	bool _updateBlacklist, _writeBlackList;
	std::map <std::string, int> blacklist;
	gz::ogzstream ignoredReads;

public:


	//construction
	TAlignmentParserBamTools();
	TAlignmentParserBamTools(int MaxReadLength, TParameters & params, TLog* Logfile);
	~TAlignmentParserBamTools();
	void init(int MaxReadLength, TParameters & params, TLog* Logfile);

        //getter
        double getPositionInFile(){ return (double) bamReader.tell() / (double) sizeOfBamFile; };
        
	//setters
	void setQualityFilters(int minQual, int maxQual);
	void setQualityRangeForPrinting(int minQual, int maxQual);
	void setReadTrimming(int trim3Prime, int trim5Prime);
	void setApplyFragmentLengthFilter(bool filterYesNo);

	void fillReferenceSequence(TFastaBuffer* fastaBuffer, TAlignment & alignment);
	std::string chrNumberToName(int chrNumber);
	int chrNumberToLength(int chrNumber);
	long calcReferenceLength();

	//functions to read and _parse
	//void checkAndFillAlingment(BamTools::BamAlignment& bamAlignment, TAlignment & alignment);
	void addReference(BamTools::Fasta* reference);
	void recalibrate(TAlignment & alignment);

	//reading data requires windows
	bool readDataInNextWindow(TWindow & window);

	//reading data only requires alignments
	bool readNextAlignment(TAlignment & alignment); //to be used to go through bam file alignment by alignment
	bool readNextAlignmentWithBlacklist(TAlignment & alignment);

	//qualityTransformation
	//void initializeRecalibrationForQualityTransformation(TParameters & params);
	void addSitesToQualityTransformTable(TAlignment & alignment, TQualityTransformTables & QTtables);
	void addSitesToQualityTransformTable(TAlignment & alignment, TRecalibration* otherRecalObject, TQualityTransformTables & QTtables);
	void mergeAlignedBasesBamReads(TAlignment* fwdAlignment, TAlignment* revAlignment, bool adaptQuality);

	//blacklist
	void setUpdateBlacklistToTrue(){
		_updateBlacklist = true;
		logfile->list("Storing ignored reads in a blacklist");
	};

	void setWriteBlacklistToFileToTrue(){
		_writeBlackList = true;
		std::string ignoredReadsFile = extractBeforeLast(filename, ".bam") + "_ignoredReads.txt.gz";
		logfile->list("Writing ignored reads to '" + ignoredReadsFile + "'");
		ignoredReads.open(ignoredReadsFile.c_str());
		if(!ignoredReads) throw "Failed to open output file '" + ignoredReadsFile + "'!";
	}

	void addToBlacklist(TAlignment & alignment, const std::string & errorMessage){
		//TODO: should check if read already exists in blackfile (could be case in paired-end data) -> remove
		blacklist.emplace(alignment.alignmentName, 1);
		if(_writeBlackList){
			if(alignment.isReverseStrand){
				ignoredReads << "Read " << alignment.alignmentName << ", rev : " << errorMessage << "\n";
			} else {
				ignoredReads << "Read " << alignment.alignmentName << ", fwd : " << errorMessage << "\n";
			}
		}
	};
	void addToBlacklist(BamTools::BamAlignment & alignment, const std::string & errorMessage){
		//TODO: should check if read already exists in blackfile (could be case in paired-end data) -> remove
		blacklist.emplace(alignment.Name, 1);
		if(_writeBlackList){
			if(alignment.IsReverseStrand()){
				ignoredReads << "Read " << alignment.Name << ", rev : " << errorMessage << "\n";
			} else {
				ignoredReads << "Read " << alignment.Name << ", fwd : " << errorMessage << "\n";
			}
		}

	};

	void addToBlacklist(std::string & alignmentName, const std::string & errorMessage){
		//TODO: should check if read already exists in blackfile (could be case in paired-end data) -> remove
		blacklist.emplace(alignmentName, 1);
		if(_writeBlackList){
			ignoredReads << "Read " << alignmentName << " : " << errorMessage << "\n";
		}
	};

	void removeFromBlacklist(TAlignment & alignment, const std::string & errorMessage){
		blacklist.erase(alignment.alignmentName);
		if(_writeBlackList){
			if(alignment.isReverseStrand){
				ignoredReads << "Read " << alignment.alignmentName << ", rev : " << errorMessage << "\n";
			} else {
				ignoredReads << "Read " << alignment.alignmentName << ", fwd : " << errorMessage << "\n";
			}
		}
	};

	bool isInBlacklist(std::string & alignmentName){
		if(blacklist.count(alignmentName) > 0)
			return true;
		return false;
	};
};


#endif /* TALIGNMENTPARSER_H_BAMTOOLS*/
